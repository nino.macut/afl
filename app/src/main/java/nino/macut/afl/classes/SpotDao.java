package nino.macut.afl.classes;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SpotDao {
    @Query("SELECT * FROM spots")
    List<Spot> getAllSpots();

    @Insert
    void insertSpot(Spot... spot);

    @Update
    void updateSpot(Spot... spot);

    @Delete
    void deleteSpot(Spot spot);
}
