package nino.macut.afl.classes;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.android.gms.maps.model.LatLng;

@Entity(tableName = "spots")
public class Spot {
    @PrimaryKey(autoGenerate = true)
    public int ID;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "latitude")
    public String latitude;
    @ColumnInfo(name = "longitude")
    public String longitude;
    @ColumnInfo(name = "location_ID")
    public int locationID;

    public Spot(String name, String latitude, String longitude, int locationID){
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.locationID = locationID;
    }
}
