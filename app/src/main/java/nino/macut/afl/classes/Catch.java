package nino.macut.afl.classes;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "catches")
public class Catch {
    @PrimaryKey(autoGenerate = true)
    public int ID;
    @ColumnInfo(name = "image")
    public String image;
    @ColumnInfo(name = "time")
    public String time;
    @ColumnInfo(name = "fisherman")
    public int fisherman;
    @ColumnInfo(name = "species")
    public int species;
    @ColumnInfo(name = "trip")
    public int trip;
    @ColumnInfo(name = "bait")
    public int bait;
    @ColumnInfo(name = "temperature")
    public float temperature;
    @ColumnInfo(name = "atmospheric_pressure")
    public float atmosphericPressure;
    @ColumnInfo(name = "wind")
    public float wind;
    @ColumnInfo(name = "sky_conditions")
    public String skyConditions;
    @ColumnInfo(name = "length")
    public float length;
    @ColumnInfo(name = "weight")
    public float weight;
    @ColumnInfo(name = "quantity")
    public int quantity;
    @ColumnInfo(name = "water_clarity")
    public String waterClarity;
    @ColumnInfo(name = "water_depth")
    public float waterDepth;
    @ColumnInfo(name = "water_temperature")
    public float waterTemperature;
    @ColumnInfo(name = "fishing_method")
    public String fishingMethod;
    @ColumnInfo(name = "released")
    public boolean released;

    public Catch(String image, String time, int fisherman, int species, int trip, int bait, float temperature, float atmosphericPressure, float wind, String skyConditions, float length, float weight, int quantity, String waterClarity, float waterDepth, float waterTemperature, String fishingMethod, boolean released){
        this.image = image;
        this.time = time;
        this.fisherman = fisherman;
        this.species = species;
        this.trip = trip;
        this.bait = bait;
        this.temperature = temperature;
        this.atmosphericPressure = atmosphericPressure;
        this.wind = wind;
        this.skyConditions = skyConditions;
        this.length = length;
        this.weight = weight;
        this.quantity = quantity;
        this.waterClarity = waterClarity;
        this.waterDepth = waterDepth;
        this.waterTemperature = waterTemperature;
        this.fishingMethod = fishingMethod;
        this.released = released;
    }

    public float getWeight(){
        return this.weight;
    }

    public float getLength(){
        return this.length;
    }
}