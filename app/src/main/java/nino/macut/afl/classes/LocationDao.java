package nino.macut.afl.classes;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LocationDao {
    @Query("SELECT * FROM locations")
    List<Location> getAllLocations();

    @Insert
    void insertLocation(Location... location);

    @Update
    void updateLocation(Location... location);

    @Delete
    void deleteLocation(Location location);

    @Transaction
    @Query("SELECT * FROM spots WHERE location_ID = :ID")
    List<Spot> getSpotsForLocation(int ID);
}
