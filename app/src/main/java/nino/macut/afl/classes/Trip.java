package nino.macut.afl.classes;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "trips")
public class Trip implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int ID;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "location")
    public int location;
    @ColumnInfo(name = "start_date")
    public String startDate;
    @ColumnInfo(name = "end_date")
    public String endDate;
    @ColumnInfo(name = "note")
    public String note;

    public Trip(String name, int location, String startDate, String endDate, String note){
        this.name = name;
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
        this.note = note;
    }
}