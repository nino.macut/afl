package nino.macut.afl.classes;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BaitCategoryDao {
    @Query("SELECT * FROM bait_categories")
    List<BaitCategory> getAllBaitCategories();

    @Insert
    void insertBaitCategory(BaitCategory... baitCategory);

    @Update
    void updateBaitCategory(BaitCategory... baitCategory);

    @Delete
    void deleteBaitCategory(BaitCategory baitCategory);
}