package nino.macut.afl.classes;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "fishermen")
public class Fisherman{
    @PrimaryKey(autoGenerate = true)
    public int ID;
    @ColumnInfo(name = "path_to_image")
    public String image;
    @ColumnInfo(name = "date_of_birth")
    public String dateOfBirth;
    @ColumnInfo(name = "first_name")
    public String firstName;
    @ColumnInfo(name = "last_name")
    public String lastName;

    public Fisherman(String image, String dateOfBirth, String firstName, String lastName){
        this.image = image;
        this.dateOfBirth = dateOfBirth;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}