package nino.macut.afl.classes;

import android.widget.GridLayout;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SpeciesDao {
    @Query("SELECT * FROM species")
    List<Species> getAllSpecies();

    @Query("SELECT * FROM species WHERE ID > :ID")
    Species[] getSpeciesById(int ID);

    @Insert
    void insertSpecies(Species... Species);

    @Update
    void updateSpecies(Species... Species);

    @Delete
    void deleteSpecies(Species species);
}