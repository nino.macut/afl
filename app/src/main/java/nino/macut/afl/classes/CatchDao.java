package nino.macut.afl.classes;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CatchDao {
    @Query("SELECT * FROM catches")
    List<Catch> getAllCatches();

    @Insert
    void insertCatch(Catch... catchy);

    @Update
    void updateCatch(Catch... catchy);

    @Delete
    void deleteCatch(Catch catchy);
}