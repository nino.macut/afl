package nino.macut.afl.classes;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface FishermanDao {
    @Query("SELECT * FROM fishermen")
    List<Fisherman> getAllFishermen();

    @Insert
    void insertFisherman(Fisherman... fishermen);

    @Update
    void updateFisherman(Fisherman... fisherman);

    @Delete
    void deleteFisherman(Fisherman fisherman);
}
