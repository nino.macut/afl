package nino.macut.afl.classes;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "species")
public class Species {
    @PrimaryKey(autoGenerate = true)
    public int ID;
    @ColumnInfo(name = "image")
    public String image;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "type")
    public String type;
    @ColumnInfo(name = "description")
    public String description;

    public Species(String image, String name, String type, String description){
        this.image = image;
        this.name = name;
        this.type = type;
        this.description = description;
    }
}