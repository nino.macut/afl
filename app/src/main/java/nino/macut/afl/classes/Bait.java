package nino.macut.afl.classes;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "baits")
public class Bait {
    @PrimaryKey(autoGenerate = true)
    public int ID;
    @ColumnInfo(name = "image")
    public String image;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "brand")
    public String brand;
    @ColumnInfo(name = "category")
    public int category;
    @ColumnInfo(name = "type")
    public String type;
    @ColumnInfo(name = "size")
    public float size;
    @ColumnInfo(name = "color")
    public String color;
    @ColumnInfo(name = "description")
    public String description;

    public Bait(String image, String name, String brand, int category, String type, float size, String color, String description){
        this.image = image;
        this.name = name;
        this.brand = brand;
        this.category = category;
        this.type = type;
        this.size = size;
        this.color = color;
        this.description = description;
    }
}