package nino.macut.afl.classes;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TripDao {
    @Query("SELECT * FROM trips")
    List<Trip> getAllTrips();

    @Insert
    void insertTrip(Trip... trip);

    @Update
    void updateTrip(Trip... trip);

    @Delete
    void deleteTrip(Trip trip);
}
