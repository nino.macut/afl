package nino.macut.afl.classes;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "bait_categories")
public class BaitCategory {
    @PrimaryKey(autoGenerate = true)
    public int ID;
    @ColumnInfo(name = "name")
    public String name;

    public BaitCategory(String name){
        this.name = name;
    }
}