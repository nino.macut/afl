package nino.macut.afl.classes;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BaitDao {
    @Query("SELECT * FROM baits")
    List<Bait> getAllBaits();

    @Insert
    void insertBait(Bait... bait);

    @Update
    void updateBait(Bait... bait);

    @Delete
    void deleteBait(Bait bait);
}