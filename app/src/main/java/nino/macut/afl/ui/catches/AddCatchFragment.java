package nino.macut.afl.ui.catches;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Species;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.bait_categories.AddBaitCategoryDialogFragment;
import nino.macut.afl.ui.baits.AddBaitDialogFragment;
import nino.macut.afl.ui.fishermen.AddFishermanDialogFragment;
import nino.macut.afl.ui.species.AddSpeciesDialogFragment;
import nino.macut.afl.ui.trips.AddTripDialogFragment;

public class AddCatchFragment extends Fragment implements LocationListener {

    private ImageView previewImage;
    private Button buttonUploadCatchImage;
    private Button buttonAddNewCatch;
    private Button buttonAddNewTrip;
    private Button buttonAddNewSpecies;
    private Button buttonAddNewBait;
    private Button buttonAddNewFisherman;
    private String fileName;
    Bundle inputBundle;

    private final String url = "http://api.openweathermap.org/data/2.5/weather";
    private final String appid = "2c4ff5d50a3a2378fc0ae481ebc36012";
    DecimalFormat df = new DecimalFormat("#.##");
    LocationManager locationManager;
    Spinner skyConditions;
    EditText temperature;
    EditText pressure;
    EditText wind;
    EditText time;

    int hour;
    int minute;

    private List<Trip> tripsList;
    private List<Bait> baitsList;
    private List<Species> speciesList;
    private List<Fisherman> fishermenList;

    int SELECT_IMAGE_CODE = 1;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_catch, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadLists();
        fillDropdowns(root);

        setDefaults(root);
        getTime(root);
        getLocation();

        //Odabir profilne slike.
        previewImage = root.findViewById(R.id.image_catch_preview);
        buttonUploadCatchImage = root.findViewById(R.id.button_upload_catch_portrait);

        buttonUploadCatchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Title"), SELECT_IMAGE_CODE);
            }
        });

        buttonAddNewTrip = root.findViewById(R.id.button_add_new_trip_from_catches);

        buttonAddNewTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTripDialogFragment df = new AddTripDialogFragment();
                df.show(getChildFragmentManager(), "AddTripDialogFragment");
            }
        });

        buttonAddNewSpecies = root.findViewById(R.id.button_add_new_species_from_catches);

        buttonAddNewSpecies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddSpeciesDialogFragment df = new AddSpeciesDialogFragment();
                df.show(getChildFragmentManager(), "AddSpeciesDialogFragment");
            }
        });

        buttonAddNewBait = root.findViewById(R.id.button_add_new_bait_from_catches);

        buttonAddNewBait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddBaitDialogFragment df = new AddBaitDialogFragment();
                df.show(getChildFragmentManager(), "AddBaitDialogFragment");
            }
        });

        buttonAddNewFisherman = root.findViewById(R.id.button_add_new_fisherman_from_catches);

        buttonAddNewFisherman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddFishermanDialogFragment df = new AddFishermanDialogFragment();
                df.show(getChildFragmentManager(), "AddFishermanDialogFragment");
            }
        });

        Spinner trip = root.findViewById(R.id.dropdown_catch_trip);

        trip.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadLists();
                    fillTripDropdown(root);
                    return false;
                }
                return false;
            }
        });

        Spinner species = root.findViewById(R.id.dropdown_catch_species);

        species.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadLists();
                    fillSpeciesDropdown(root);
                    return false;
                }
                return false;
            }
        });

        Spinner bait = root.findViewById(R.id.dropdown_catch_bait);

        bait.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadLists();
                    fillBaitDropdown(root);
                    return false;
                }
                return false;
            }
        });

        Spinner fisherman = root.findViewById(R.id.dropdown_catch_fisherman);

        fisherman.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadLists();
                    fillFishermenDropdown(root);
                    return false;
                }
                return false;
            }
        });

        time = root.findViewById(R.id.text_catch_time_input);

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popTimePicker(root);
            }
        });

        //Kreiranje i unos lokacije u text file.
        buttonAddNewCatch = root.findViewById(R.id.button_add_new_catch);

        buttonAddNewCatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dohvati podatke.
                Bitmap bitmap;
                Boolean hasPicture = false;
                try{
                    BitmapDrawable image = (BitmapDrawable)previewImage.getDrawable();
                    bitmap = image.getBitmap();
                    hasPicture = true;
                }
                catch (Exception e){
                    e.printStackTrace();
                    bitmap = null;
                    hasPicture = false;
                }

                int fishermanID;
                Spinner fisherman = root.findViewById(R.id.dropdown_catch_fisherman);
                if (fisherman.getSelectedItem() != null) {
                    fishermanID = fishermenList.get(fisherman.getSelectedItemPosition()).ID;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_fisherman), Toast.LENGTH_SHORT).show();
                    return;
                }

                int tripID;
                Spinner trip = root.findViewById(R.id.dropdown_catch_trip);
                if (trip.getSelectedItem() != null) {
                    tripID = tripsList.get(trip.getSelectedItemPosition()).ID;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_trip), Toast.LENGTH_SHORT).show();
                    return;
                }

                int baitID;
                Spinner bait = root.findViewById(R.id.dropdown_catch_bait);
                if (bait.getSelectedItem() != null) {
                    baitID = baitsList.get(bait.getSelectedItemPosition()).ID;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_bait), Toast.LENGTH_SHORT).show();
                    return;
                }

                int speciesID;
                Spinner species = root.findViewById(R.id.dropdown_catch_species);
                if (species.getSelectedItem() != null) {
                    speciesID = speciesList.get(species.getSelectedItemPosition()).ID;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_species), Toast.LENGTH_SHORT).show();
                    return;
                }

                Spinner fishingMethod = root.findViewById(R.id.dropdown_catch_fishing_method);
                String fishingMethodString = fishingMethod.getSelectedItem().toString();

                Spinner skyConditions = root.findViewById(R.id.dropdown_catch_sky_conditions);
                String skyConditionsString = skyConditions.getSelectedItem().toString();

                Spinner waterClarity = root.findViewById(R.id.dropdown_catch_water_clarity);
                String waterClarityString = waterClarity.getSelectedItem().toString();

                EditText temperature = root.findViewById(R.id.text_catch_temperature_input);

                float temperatureFloat;
                if (!temperature.getText().toString().isEmpty()){
                    temperatureFloat = Float.parseFloat(temperature.getText().toString());
                } else {
                    temperatureFloat = 0;
                }

                EditText atmosphericPressure = root.findViewById(R.id.text_catch_atmospheric_pressure_input);

                float atmosphericPressureFloat;
                if (!atmosphericPressure.getText().toString().isEmpty()){
                    atmosphericPressureFloat = Float.parseFloat(atmosphericPressure.getText().toString());
                } else {
                    atmosphericPressureFloat = 0;
                }

                EditText wind = root.findViewById(R.id.text_catch_wind_input);

                float windFloat;
                if (!wind.getText().toString().isEmpty()){
                    windFloat = Float.parseFloat(wind.getText().toString());
                } else {
                    windFloat = 0;
                }

                EditText length = root.findViewById(R.id.text_catch_length_input);

                float lengthFloat;
                if (!length.getText().toString().isEmpty()){
                    lengthFloat = Float.parseFloat(length.getText().toString());
                } else {
                    lengthFloat = 0;
                }

                EditText weight = root.findViewById(R.id.text_catch_weight_input);

                float weightFloat;
                if (!weight.getText().toString().isEmpty()){
                    weightFloat = Float.parseFloat(weight.getText().toString());
                } else {
                    weightFloat = 0;
                }

                EditText waterDepth = root.findViewById(R.id.text_catch_water_depth);

                float waterDepthFloat;
                if (!waterDepth.getText().toString().isEmpty()){
                    waterDepthFloat = Float.parseFloat(waterDepth.getText().toString());
                } else {
                    waterDepthFloat = 0;
                }

                EditText quantity = root.findViewById(R.id.text_catch_quantity_input);

                int quantityInt;
                if (!quantity.getText().toString().isEmpty()){
                    quantityInt = Integer.parseInt(quantity.getText().toString());
                } else {
                    quantityInt = 0;
                }

                EditText waterTemperature = root.findViewById(R.id.text_catch_water_temperature);

                float waterTemperatureFloat;
                if (!waterTemperature.getText().toString().isEmpty()){
                    waterTemperatureFloat = Float.parseFloat(waterTemperature.getText().toString());
                } else {
                    waterTemperatureFloat = 0;
                }

                CheckBox released = root.findViewById(R.id.checkbox_catch_released);
                boolean releasedBoolean = released.isChecked();

                //Spremi sliku u internu memoriju.
                String fileName = "catch" + getRandomNumber(0,100000) + ".jpg";

                if(hasPicture == true){
                    saveToInternalStorage(bitmap, fileName);
                }
                else if (hasPicture == false){
                    fileName = "nofile";
                }

                EditText time = root.findViewById(R.id.text_catch_time_input);
                String timeString = time.getText().toString();

                addNewCatch(fileName, timeString, speciesID, fishermanID, tripID, baitID, temperatureFloat, atmosphericPressureFloat, windFloat, skyConditionsString, lengthFloat, weightFloat, quantityInt, waterClarityString, waterDepthFloat, waterTemperatureFloat, fishingMethodString, releasedBoolean);

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_catches);
            }
        });
        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        tripsList = db.tripDao().getAllTrips();
        speciesList = db.speciesDao().getAllSpecies();
        baitsList = db.baitDao().getAllBaits();
        fishermenList = db.fishermanDao().getAllFishermen();
    }

    private void addNewCatch(String image, String time, int species, int fisherman, int trip, int bait, float temperature, float atmosphericPressure, float wind, String skyConditions, float length, float weight, int quantity, String waterClarity, float waterDepth, float waterTemperature, String fishingMethod, boolean released){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());

        Catch catchy = new Catch(image, time, fisherman, species, trip, bait, temperature, atmosphericPressure, wind, skyConditions, length, weight, quantity, waterClarity, waterDepth, waterTemperature, fishingMethod, released);
        db.catchDao().insertCatch(catchy);
    }

    private void fillTripDropdown(View root){
        Spinner tripsDropdown = root.findViewById(R.id.dropdown_catch_trip);
        String[] items = new String[tripsList.size()];

        int i = 0;
        for (Trip t: tripsList
        ) {
            items[i] = t.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        tripsDropdown.setAdapter(adapter);
    }

    private void fillFishermenDropdown(View root){
        Spinner fishermenDropdown = root.findViewById(R.id.dropdown_catch_fisherman);
        String[] items = new String[fishermenList.size()];

        int i = 0;
        for (Fisherman f: fishermenList
        ) {
            items[i] = f.firstName + " " + f.lastName;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        fishermenDropdown.setAdapter(adapter);
    }

    private void fillBaitDropdown(View root){
        Spinner baitsDropdown = root.findViewById(R.id.dropdown_catch_bait);
        String[] items = new String[baitsList.size()];

        int i = 0;
        for (Bait b: baitsList
        ) {
            items[i] = b.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        baitsDropdown.setAdapter(adapter);
    }

    private void fillSpeciesDropdown(View root){
        Spinner speciesDropdown = root.findViewById(R.id.dropdown_catch_species);
        String[] items = new String[speciesList.size()];

        int i = 0;
        for (Species s: speciesList
        ) {
            items[i] = s.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        speciesDropdown.setAdapter(adapter);
    }

    private void fillWaterClarityDropdown(View root){
        Spinner waterClarityDropdown = root.findViewById(R.id.dropdown_catch_water_clarity);
        String[] items = getActivity().getResources().getStringArray(R.array.array_water_clarity);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        waterClarityDropdown.setAdapter(adapter);
    }

    private void fillSkyConditionsDropdown(View root){
        Spinner skyConditionsDropdown = root.findViewById(R.id.dropdown_catch_sky_conditions);
        String[] items = getActivity().getResources().getStringArray(R.array.array_sky_conditions);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        skyConditionsDropdown.setAdapter(adapter);
    }

    private void fillFishingMethodDropdown(View root){
        Spinner fishingMethodDropdown = root.findViewById(R.id.dropdown_catch_fishing_method);
        String[] items = getActivity().getResources().getStringArray(R.array.array_fishing_methods);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        fishingMethodDropdown.setAdapter(adapter);
    }

    private void fillDropdowns(View root){
        fillFishermenDropdown(root);
        fillBaitDropdown(root);
        fillFishingMethodDropdown(root);
        fillSkyConditionsDropdown(root);
        fillSpeciesDropdown(root);
        fillTripDropdown(root);
        fillWaterClarityDropdown(root);
    }

    private int getRandomNumber(int min,int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String fileName){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Catches", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    //Handler upita.
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_IMAGE_CODE){
            Uri uri = data.getData();
            previewImage.setImageURI(uri);
        }
    }

    //Automatsko dohvaćanje vremena
    @SuppressLint("MissingPermission")
    public void getLocation(){
        try {
            locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getWeatherDetails(Double lat, Double lng){
        String tempUrl = "";

        tempUrl = url + "?lat=" + lat + "&lon=" + lng + "&appid=" + appid;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, tempUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", response);
                String output = "";
                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    JSONArray jsonArray = jsonResponse.getJSONArray("weather");
                    JSONObject jsonObjectWeather = jsonArray.getJSONObject(0);

                    JSONObject jsonObjectMain = jsonResponse.getJSONObject("main");

                    double temp = jsonObjectMain.getDouble("temp") - 273.15;
                    temperature.setText(df.format(temp));

                    float pres = jsonObjectMain.getInt("pressure");
                    pressure.setText(df.format(pres));

                    JSONObject jsonObjectWind = jsonResponse.getJSONObject("wind");
                    String wnd = jsonObjectWind.getString("speed"); //m/s
                    float windS = Float.parseFloat(wnd);
                    double ws = windS * 3.6;
                    wind.setText(df.format(ws));

                    JSONObject jsonObjectClouds = jsonResponse.getJSONObject("clouds");
                    String clouds = jsonObjectClouds.getString("all"); //cloudyness in percentage
                    int cld = Integer.parseInt(clouds);

                    if (cld >= 0 && cld <= 20){
                        skyConditions.setSelection(0);
                    } else if (cld > 20 && cld <= 40) {
                        skyConditions.setSelection(1);
                    } else if (cld > 40 && cld <= 60) {
                        skyConditions.setSelection(2);
                    } else if (cld > 60 && cld <= 80) {
                        skyConditions.setSelection(3);
                    } else {
                        skyConditions.setSelection(4);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), error.toString().trim(), Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        Log.d("response", "test");
        requestQueue.add(stringRequest);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        Log.d("response", String.valueOf(lat));
        Log.d("response", String.valueOf(lng));

        getWeatherDetails(lat, lng);
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

    public void setDefaults(View root){
        skyConditions = root.findViewById(R.id.dropdown_catch_sky_conditions);
        temperature = root.findViewById(R.id.text_catch_temperature_input);
        pressure = root.findViewById(R.id.text_catch_atmospheric_pressure_input);
        wind = root.findViewById(R.id.text_catch_wind_input);
    }

    public void popTimePicker(View root){
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
                hour = selectedHour;
                minute = selectedMinute;
                time.setText(String.format(Locale.getDefault(), "%02d:%02d",hour,minute));
            }
        };


        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.DialogTheme, onTimeSetListener, hour, minute, true);

        timePickerDialog.setTitle(R.string.input_select_time);
        timePickerDialog.show();
    }

    public void getTime(View root){
        EditText timeText = root.findViewById(R.id.text_catch_time_input);
        Calendar calendar = Calendar.getInstance();
        timeText.setText(String.format(Locale.getDefault(), "%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE)));
    }
}