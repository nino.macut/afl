package nino.macut.afl.ui.map;

import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Location;
import nino.macut.afl.db.AppDatabase;


public class MapFragment extends Fragment {

    List<Location> locationsList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_map, container, false);

        loadLocationsList();

        //Initialize map fragment
        SupportMapFragment supportMapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.google_maps_all_locations);

        //Async map
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull GoogleMap googleMap) {
                //When map is loaded
                try{
                    boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.mapstyle));
                } catch (Resources.NotFoundException e){

                }

                googleMap.clear();

                for (Location l: locationsList){
                    Double lat = Double.parseDouble(l.latitude);
                    Double lng = Double.parseDouble(l.longitude);

                    LatLng initialPosition = new LatLng(lat,lng);

                    MarkerOptions markerOptions = new MarkerOptions();
                    //Set position of marker
                    markerOptions.position(initialPosition);
                    //Set title of marker
                    markerOptions.title(l.name);
                    //Remove all markers
                    googleMap.addMarker(markerOptions);
                }
            }
        });

        return root;
    }

    private void loadLocationsList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        locationsList = db.locationDao().getAllLocations();
    }
}