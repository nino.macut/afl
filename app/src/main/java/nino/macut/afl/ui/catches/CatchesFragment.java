package nino.macut.afl.ui.catches;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.Species;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.baits.BaitsFragment;

public class CatchesFragment extends Fragment {

    private List<Catch> catchesList;
    private ListView listOfCatches;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_catches, container, false);

        verifyPermissions();
        loadCatchesList();
        initializeList(root);
        initializeAddButton(root);

        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadCatchesList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        catchesList = db.catchDao().getAllCatches();
    }

    private void loadList(){
        CatchesAdapter catchesAdapter = new CatchesAdapter(getActivity(), catchesList);
        listOfCatches.setAdapter(catchesAdapter);
    }

    private void initializeList(View root){
        listOfCatches = root.findViewById(R.id.list_catches);
        listOfCatches.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt("editID", position);
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_edit_catch, bundle);
            }
        });
        loadList();
    }

    private void initializeAddButton(View root){
        Button buttonAddCatch = root.findViewById(R.id.button_add_catch);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        buttonAddCatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_add_catch);
            }
        });
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir("Catches", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public class CatchesAdapter extends ArrayAdapter<Catch> {
        public CatchesAdapter(Context context, List<Catch> catchesList){
            super(context, R.layout.listitem_catch, catchesList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Catch catchy = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_catch,parent,false);
            }

            ImageView image = convertView.findViewById(R.id.image_catch_image_listitem);
            TextView species = convertView.findViewById(R.id.text_catch_species_listitem);
            TextView dateAndTime = convertView.findViewById(R.id.text_catch_date_listitem);
            TextView location = convertView.findViewById(R.id.text_catch_location_listitem);

            if (!catchy.image.equals("nofile")){
                Bitmap bitmap = loadImageFromStorage(catchy.image);
                image.setImageBitmap(bitmap);
            }
            else if (catchy.image.equals("nofile")){
                image.setImageResource(R.drawable.ic_catches_no_pic);
            }

            AppDatabase db = AppDatabase.getDbInstance(getActivity());
            List<Species> speciesList = db.speciesDao().getAllSpecies();
            List<Location> locationsList = db.locationDao().getAllLocations();
            List<Trip> tripsList = db.tripDao().getAllTrips();
            List<Fisherman> fishermenList = db.fishermanDao().getAllFishermen();

            for(Species s : speciesList){
                if (s.ID == catchy.species){
                    species.setText(s.name);
                }
            }

            String locTemp = "";
            for(Trip t : tripsList){
                if (t.ID == catchy.trip){
                    locTemp = locationsList.get(t.location).name;
                    dateAndTime.setText(t.startDate + " - " + catchy.time);
                }
            }

            for(Fisherman f : fishermenList){
                if (f.ID == catchy.fisherman){
                    locTemp += ", " + f.firstName + " " + f.lastName;
                }
            }

            location.setText(locTemp);

            return convertView;
        }
    }
}