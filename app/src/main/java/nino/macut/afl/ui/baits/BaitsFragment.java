package nino.macut.afl.ui.baits;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.fishermen.FishermenFragment;

public class BaitsFragment extends Fragment {

    private List<Bait> baitsList;
    private ListView listOfBaits;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_baits, container, false);

        verifyPermissions();
        loadBaitsList();
        initializeList(root);
        initializeAddButton(root);

        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadBaitsList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        baitsList = db.baitDao().getAllBaits();
    }

    private void loadList(){
        BaitsAdapter baitsAdapter = new BaitsAdapter(getActivity(), baitsList);
        listOfBaits.setAdapter(baitsAdapter);
    }

    private void initializeList(View root){
        listOfBaits = root.findViewById(R.id.list_baits);
        listOfBaits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt("editID", position);
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_edit_bait, bundle);
            }
        });
        loadList();
    }

    private void initializeAddButton(View root){
        Button buttonAddBait = root.findViewById(R.id.button_add_bait);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        buttonAddBait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_add_bait);
            }
        });
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir("Baits", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public class BaitsAdapter extends ArrayAdapter<Bait> {
        public BaitsAdapter(Context context, List<Bait> baitsList){
            super(context, R.layout.listitem_bait, baitsList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Bait bait = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_bait,parent,false);
            }

            ImageView image = convertView.findViewById(R.id.image_bait_image_listitem);
            TextView brand = convertView.findViewById(R.id.text_bait_brand_listitem);
            TextView name = convertView.findViewById(R.id.text_bait_name_listitem);
            TextView category = convertView.findViewById(R.id.text_bait_category_listitem);

            if (!bait.image.equals("nofile")){
                Bitmap bitmap = loadImageFromStorage(bait.image);
                image.setImageBitmap(bitmap);
            }
            else if (bait.image.equals("nofile")){
                image.setImageResource(R.drawable.ic_baits_no_pic);
            }


            brand.setText(bait.brand + "");
            name.setText(bait.name + "");

            AppDatabase db = AppDatabase.getDbInstance(getActivity());
            List<BaitCategory> baitCategoriesList = db.baitCategoryDao().getAllBaitCategories();

            if (bait.size != 0.0f){
                category.setText(baitCategoriesList.get(bait.category).name + ", " + bait.color + ", " + bait.type + ", " + bait.size + " " + getString(R.string.unit_centimeters));
            } else if (bait.size == 0.0f){
                category.setText(baitCategoriesList.get(bait.category).name + ", " + bait.color + ", " + bait.type);
            }


            return convertView;
        }
    }
}