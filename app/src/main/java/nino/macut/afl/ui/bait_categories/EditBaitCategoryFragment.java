package nino.macut.afl.ui.bait_categories;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.db.AppDatabase;

public class EditBaitCategoryFragment extends Fragment {

    private int editID;
    private List<BaitCategory> baitCategoriesList;
    private Button buttonDeleteBaitCategory;
    private Button buttonEditBaitCategory;
    private Bundle bundle;

    boolean argumentsRead = false;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_bait_category, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadBaitCategoriesList();


        if(!argumentsRead){

            bundle = this.getArguments();
            if (bundle != null) {
                editID = bundle.getInt("editID");
                BaitCategory baitCategory = baitCategoriesList.get(editID);

                EditText name = root.findViewById(R.id.text_bait_category_name_input);
                name.setText(baitCategory.name);
            }

            argumentsRead = true;
        }

        //Brisanje ribiča
        buttonDeleteBaitCategory = root.findViewById(R.id.button_delete_bait_category);

        buttonDeleteBaitCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle(R.string.menu_confirm);
                builder.setMessage(R.string.message_are_you_sure_delete);

                builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        BaitCategory baitCategory = baitCategoriesList.get(editID);
                        removeBaitCategoryFromDB(baitCategory);
                        dialog.dismiss();

                        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                        navController.navigate(R.id.nav_bait_categories);
                    }
                });

                builder.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        //Kreiranje i unos fisherman-a u text file.
        buttonEditBaitCategory = root.findViewById(R.id.button_edit_bait_category);

        buttonEditBaitCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText name = root.findViewById(R.id.text_bait_category_name_input);
                String nameString = name.getText().toString();

                if (TextUtils.isEmpty(name.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_bait_category_name), Toast.LENGTH_SHORT).show();
                    return;
                }

                BaitCategory baitCategory = baitCategoriesList.get(editID);

                baitCategory.name = nameString;

                editBaitCategoryInDB(baitCategory);

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_bait_categories);
            }
        });
        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadBaitCategoriesList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        baitCategoriesList = db.baitCategoryDao().getAllBaitCategories();
    }

    private void editBaitCategoryInDB(BaitCategory baitCategory){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.baitCategoryDao().updateBaitCategory(baitCategory);
    }

    private void removeBaitCategoryFromDB(BaitCategory baitCategory){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.baitCategoryDao().deleteBaitCategory(baitCategory);
    }
}