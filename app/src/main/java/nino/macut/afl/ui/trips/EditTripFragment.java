package nino.macut.afl.ui.trips;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;


public class EditTripFragment extends Fragment {

    Button buttonEditTrip;
    Button buttonDeleteTrip;
    List<Trip> tripsList;
    List<Location> locationsList;
    int editID;

    private DatePickerDialog.OnDateSetListener startDateSetListener;
    private DatePickerDialog.OnDateSetListener endDateSetListener;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_trip, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadLists();
        fillLocationDropdown(root);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            editID = bundle.getInt("editID");
            Trip trip = tripsList.get(editID);

            EditText name = root.findViewById(R.id.text_trip_name_edit);
            name.setText(trip.name);

            Spinner locationDropdown = root.findViewById(R.id.dropdown_trip_location_edit);
            locationDropdown.setSelection(((ArrayAdapter)locationDropdown.getAdapter()).getPosition(locationsList.get(trip.location).name));

            EditText startDate = root.findViewById(R.id.edit_date_trip_start_date);
            startDate.setText(trip.startDate);

            EditText endDate = root.findViewById(R.id.edit_date_trip_end_date);
            endDate.setText(trip.endDate);

            EditText note = root.findViewById(R.id.text_trip_note_edit);
            note.setText(trip.note);
        }

        buttonDeleteTrip = root.findViewById(R.id.button_delete_trip);

        buttonDeleteTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle(R.string.menu_confirm);
                builder.setMessage(R.string.message_are_you_sure_delete);

                builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Trip trip = tripsList.get(editID);
                        removeTripFromDB(trip);
                        dialog.dismiss();
                        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                        navController.navigate(R.id.nav_trips);
                    }
                });

                builder.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        EditText startDate = root.findViewById(R.id.edit_date_trip_start_date);

        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DateDialogTheme, startDateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        startDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                startDate.setText("" + dayOfMonth + "/" + (month + 1) + "/" + year);
            }
        };

        //Odabir datuma kraja.
        EditText endDate = root.findViewById(R.id.edit_date_trip_end_date);

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DateDialogTheme, endDateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        endDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                endDate.setText("" + dayOfMonth + "/" + (month + 1) + "/" + year);
            }
        };

        buttonEditTrip = root.findViewById(R.id.button_edit_trip);

        buttonEditTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText name = root.findViewById(R.id.text_trip_name_edit);
                String nameString = name.getText().toString();

                int locationID;
                Spinner location = root.findViewById(R.id.dropdown_trip_location_edit);
                if (location.getSelectedItem() != null) {
                    locationID = location.getSelectedItemPosition();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_location), Toast.LENGTH_SHORT).show();
                    return;
                }

                EditText startDate = root.findViewById(R.id.edit_date_trip_start_date);
                String startDateString = startDate.getText().toString();

                EditText endDate = root.findViewById(R.id.edit_date_trip_end_date);
                String endDateString = endDate.getText().toString();

                EditText note = root.findViewById(R.id.text_trip_note_edit);
                String noteString = note.getText().toString();

                if (TextUtils.isEmpty(name.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_trip_name), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(startDate.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_start_date), Toast.LENGTH_SHORT).show();
                    return;
                }

                Trip trip = tripsList.get(editID);

                trip.name = nameString;
                trip.location = locationID;
                trip.startDate = startDateString;
                trip.endDate = endDateString;
                trip.note = noteString;

                editTripInDB(trip);

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_trips);

            }
        });
        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        tripsList = db.tripDao().getAllTrips();
        locationsList = db.locationDao().getAllLocations();
    }

    private void editTripInDB(Trip trip){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.tripDao().updateTrip(trip);
    }

    private void removeTripFromDB(Trip trip){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.tripDao().deleteTrip(trip);
    }

    private void fillLocationDropdown(View root){
        Spinner locationsDropdown = root.findViewById(R.id.dropdown_trip_location_edit);
        String[] items = new String[locationsList.size()];

        int i = 0;
        for (Location l: locationsList
        ) {
            items[i] = l.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        locationsDropdown.setAdapter(adapter);
    }
}