package nino.macut.afl.ui.locations;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.fishermen.AddFishermanFragment;

public class LocationsFragment extends Fragment {

    public static LocationsFragment newInstance() {
        return new LocationsFragment();
    }

    private List<Location> locationsList;
    private List<Trip> tripsList;
    private ListView listOfLocations;
    private List<Catch> catchesList;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_locations, container, false);

        verifyPermissions();
        loadLists();
        initializeList(root);
        initializeAddButton(root);

        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        locationsList = db.locationDao().getAllLocations();
        tripsList = db.tripDao().getAllTrips();
        catchesList = db.catchDao().getAllCatches();
    }

    private void loadList(){
        LocationsAdapter locationsAdapter = new LocationsAdapter(getActivity(), locationsList);
        listOfLocations.setAdapter(locationsAdapter);
    }

    private void initializeList(View root){
        listOfLocations = root.findViewById(R.id.list_locations);
        listOfLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt("editID", position);
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_edit_location, bundle);
            }
        });
        loadList();
    }

    private void initializeAddButton(View root){
        Button buttonAddLocation = root.findViewById(R.id.button_add_location);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        buttonAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_add_location);
            }
        });
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Locations", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public class LocationsAdapter extends ArrayAdapter<Location> {
        public LocationsAdapter(Context context, List<Location> locationList){
            super(context, R.layout.listitem_location, locationList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Location location = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_location,parent,false);
            }

            ImageView image = convertView.findViewById(R.id.image_location_preview_listitem);
            TextView name = convertView.findViewById(R.id.text_location_name_listitem);
            TextView type = convertView.findViewById(R.id.text_location_type_listitem);
            TextView catches = convertView.findViewById(R.id.text_location_catch_count_listitem);

            int numOfCatches = 0;
            for (Catch c : catchesList){
                int tripIndex = 0;
                for (Trip t : tripsList){
                    if (t.ID == c.trip){
                        break;
                    }
                    tripIndex++;
                }
                if (locationsList.get(tripsList.get(tripIndex).location).name == location.name){
                    numOfCatches++;
                }
            }
            catches.setText(getString(R.string.listitem_catches) + " " + numOfCatches);

            if (!location.image.equals("nofile")){
                Bitmap bitmap = loadImageFromStorage(location.image);
                image.setImageBitmap(bitmap);
            }
            else if (location.image.equals("nofile")){
                image.setImageResource(R.drawable.ic_location_no_pic);
            }

            name.setText(location.name + "");
            type.setText(location.type);
            return convertView;
        }
    }
}