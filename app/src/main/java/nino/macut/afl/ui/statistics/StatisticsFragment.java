package nino.macut.afl.ui.statistics;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.Species;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;

public class StatisticsFragment extends Fragment {

    private List<Trip> tripsList;
    private List<Bait> baitsList;
    private List<Species> speciesList;
    private List<Fisherman> fishermenList;
    private List<Catch> catchesList;
    private List<Location> locationsList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_statistics, container, false);

        loadLists();
        fillFishermenDropdown(root);

        showStatistics(root, getString(R.string.statistics_no_fisherman));

        Spinner fishermen = root.findViewById(R.id.dropdown_statistics_fisherman);

        fishermen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //resetStatistics();
                showStatistics(root, fishermen.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return root;
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        tripsList = db.tripDao().getAllTrips();
        speciesList = db.speciesDao().getAllSpecies();
        baitsList = db.baitDao().getAllBaits();
        fishermenList = db.fishermanDao().getAllFishermen();
        catchesList = db.catchDao().getAllCatches();
        locationsList = db.locationDao().getAllLocations();
    }

    private void fillFishermenDropdown(View root){
        Spinner fishermenDropdown = root.findViewById(R.id.dropdown_statistics_fisherman);
        String[] items = new String[fishermenList.size() + 1];

        items[0] = getString(R.string.statistics_no_fisherman);

        int i = 1;
        for (Fisherman f: fishermenList
        ) {
            items[i] = (f.firstName + " " + f.lastName);
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        fishermenDropdown.setAdapter(adapter);
    }

    private void showStatistics(View root, String query){

        getSpeciesCaught(root, query);
        getBaitsUsed(root, query);
        getCatchesByLocation(root, query);
        getLongestCatch(root, query);
        getHeaviestCatch(root, query);

    }

    private void getSpeciesCaught(View root, String query){
        //Species Caught
        Map<String, Integer> speciesCaught = new HashMap<String, Integer>();

        if(query == getString(R.string.statistics_no_fisherman)){
            for(Catch c : catchesList){
                for(Species s : speciesList){
                    if (s.ID == c.species){
                        if (speciesCaught.containsKey(s.name)){
                            speciesCaught.put(s.name, speciesCaught.get(s.name) + c.quantity);
                            continue;
                        } else {
                            speciesCaught.put(s.name, 1);
                            continue;
                        }
                    }
                }
            }
        } else {
            Log.d("test", "getSpeciesCaught: " + query);


            Fisherman fisherman = null;
            for (Fisherman f : fishermenList){
                if ((f.firstName + " " + f.lastName).equals(query)){
                    fisherman = f;
                    break;
                }
            }

            for(Catch c : catchesList){
                for(Species s : speciesList){
                    if ((s.ID == c.species) && (fisherman.ID == c.fisherman)){
                        if (speciesCaught.containsKey(s.name)){
                            speciesCaught.put(s.name, speciesCaught.get(s.name) + c.quantity);
                            continue;
                        } else {
                            speciesCaught.put(s.name, 1);
                            continue;
                        }
                    }
                }
            }
        }

        LinkedHashMap<String, Integer> speciesCaughtSorted = new LinkedHashMap<>();

        //Use Comparator.reverseOrder() for reverse ordering
        speciesCaught.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> speciesCaughtSorted.put(x.getKey(), x.getValue()));

        List<String> speciesCaughtstrings = new ArrayList<String>(speciesCaughtSorted.keySet());
        List<Integer> speciesCaughtintegers = new ArrayList<Integer>(speciesCaughtSorted.values());

        TextView firstSpeciesName = root.findViewById(R.id.statistics_species_caught_1_name);
        TextView firstSpeciesQuantity = root.findViewById(R.id.statistics_species_caught_1_quantity);
        if (1 > speciesCaughtstrings.size()){
            firstSpeciesName.setVisibility(View.GONE);
            firstSpeciesQuantity.setVisibility(View.GONE);
        } else {
            firstSpeciesName.setVisibility(View.VISIBLE);
            firstSpeciesQuantity.setVisibility(View.VISIBLE);
            firstSpeciesName.setText(speciesCaughtstrings.get(0));
            firstSpeciesQuantity.setText(speciesCaughtintegers.get(0).toString());
        }

        TextView secondSpeciesName = root.findViewById(R.id.statistics_species_caught_2_name);
        TextView secondSpeciesQuantity = root.findViewById(R.id.statistics_species_caught_2_quantity);

        if (2 > speciesCaughtstrings.size()){
            secondSpeciesName.setVisibility(View.GONE);
            secondSpeciesQuantity.setVisibility(View.GONE);
        } else {
            secondSpeciesName.setVisibility(View.VISIBLE);
            secondSpeciesQuantity.setVisibility(View.VISIBLE);
            secondSpeciesName.setText(speciesCaughtstrings.get(1));
            secondSpeciesQuantity.setText(speciesCaughtintegers.get(1).toString());
        }

        TextView thirdSpeciesName = root.findViewById(R.id.statistics_species_caught_3_name);
        TextView thirdSpeciesQuantity = root.findViewById(R.id.statistics_species_caught_3_quantity);
        if (3 > speciesCaughtstrings.size()){
            thirdSpeciesName.setVisibility(View.GONE);
            thirdSpeciesQuantity.setVisibility(View.GONE);
        } else {
            thirdSpeciesName.setVisibility(View.VISIBLE);
            thirdSpeciesQuantity.setVisibility(View.VISIBLE);
            thirdSpeciesName.setText(speciesCaughtstrings.get(2));
            thirdSpeciesQuantity.setText(speciesCaughtintegers.get(2).toString());
        }

        Button showMoreSpeciesCatches = root.findViewById(R.id.button_statistics_show_more_species_caught);

        showMoreSpeciesCatches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

                Bundle bundle = new Bundle();

                ArrayList<Integer> speciesCatchesIntsArray = new ArrayList<Integer>();
                speciesCatchesIntsArray.addAll(speciesCaughtintegers);

                ArrayList<String> speciesCatchesStrsArray = new ArrayList<String>();
                speciesCatchesStrsArray.addAll(speciesCaughtstrings);

                bundle.putIntegerArrayList("values", speciesCatchesIntsArray);
                bundle.putStringArrayList("strings", speciesCatchesStrsArray);
                bundle.putString("chartName", getString(R.string.chart_catches_by_species));

                navController.navigate(R.id.nav_pie_chart, bundle);
            }
        });
    }

    private void getBaitsUsed(View root, String query){
        //BaitsUsed
        Map<String, Integer> baitsUsed = new HashMap<String, Integer>();

        if(query == getString(R.string.statistics_no_fisherman)){
            for(Catch c : catchesList){
                for(Bait b : baitsList){
                    if (b.ID == c.bait){
                        if (baitsUsed.containsKey(b.name)){
                            baitsUsed.put(b.name, baitsUsed.get(b.name) + 1);
                            continue;
                        } else {
                            baitsUsed.put(b.name, 1);
                            continue;
                        }
                    }
                }
            }
        } else {
            Log.d("test", "getBaitsUsed: " + query);

            Fisherman fisherman = null;
            for (Fisherman f : fishermenList){
                if ((f.firstName + " " + f.lastName).equals(query)){
                    fisherman = f;
                    break;
                }
            }

            for(Catch c : catchesList){
                for(Bait b  : baitsList){
                    if ((b.ID == c.bait) && (fisherman.ID == c.fisherman)){
                        if (baitsUsed.containsKey(b.name)){
                            baitsUsed.put(b.name, baitsUsed.get(b.name) + 1);
                            continue;
                        } else {
                            baitsUsed.put(b.name, 1);
                            continue;
                        }
                    }
                }
            }
        }

        LinkedHashMap<String, Integer> baitsUsedSorted = new LinkedHashMap<>();

        //Use Comparator.reverseOrder() for reverse ordering
        baitsUsed.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> baitsUsedSorted.put(x.getKey(), x.getValue()));

        List<String> baitsUsedstrings = new ArrayList<String>(baitsUsedSorted.keySet());
        List<Integer> baitsUsedintegers = new ArrayList<Integer>(baitsUsedSorted.values());

        TextView firstBaitName = root.findViewById(R.id.statistics_bait_used_1_name);
        TextView firstBaitQuantity = root.findViewById(R.id.statistics_bait_used_1_quantity);
        if (1 > baitsUsedstrings.size()){
            firstBaitName.setVisibility(View.GONE);
            firstBaitQuantity.setVisibility(View.GONE);
        } else {
            firstBaitName.setVisibility(View.VISIBLE);
            firstBaitQuantity.setVisibility(View.VISIBLE);
            firstBaitName.setText(baitsUsedstrings.get(0));
            firstBaitQuantity.setText(baitsUsedintegers.get(0).toString());
        }

        TextView secondBaitName = root.findViewById(R.id.statistics_bait_used_2_name);
        TextView secondBaitQuantity = root.findViewById(R.id.statistics_bait_used_2_quantity);

        if (2 > baitsUsedstrings.size()){
            secondBaitName.setVisibility(View.GONE);
            secondBaitQuantity.setVisibility(View.GONE);
        } else {
            secondBaitName.setVisibility(View.VISIBLE);
            secondBaitQuantity.setVisibility(View.VISIBLE);
            secondBaitName.setText(baitsUsedstrings.get(1));
            secondBaitQuantity.setText(baitsUsedintegers.get(1).toString());
        }

        TextView thirdBaitName = root.findViewById(R.id.statistics_bait_used_3_name);
        TextView thirdBaitQuantity = root.findViewById(R.id.statistics_bait_used_3_quantity);
        if (3 > baitsUsedstrings.size()){
            thirdBaitName.setVisibility(View.GONE);
            thirdBaitQuantity.setVisibility(View.GONE);
        } else {
            thirdBaitName.setVisibility(View.VISIBLE);
            thirdBaitQuantity.setVisibility(View.VISIBLE);
            thirdBaitName.setText(baitsUsedstrings.get(2));
            thirdBaitQuantity.setText(baitsUsedintegers.get(2).toString());
        }

        Button showMoreBaitsUsed = root.findViewById(R.id.button_statistics_show_more_baits_used);

        showMoreBaitsUsed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

                Bundle bundle = new Bundle();

                ArrayList<Integer> baitsUsedIntsArray = new ArrayList<Integer>();
                baitsUsedIntsArray.addAll(baitsUsedintegers);

                ArrayList<String> baitsUsedStrsArray = new ArrayList<String>();
                baitsUsedStrsArray.addAll(baitsUsedstrings);

                bundle.putIntegerArrayList("values", baitsUsedIntsArray);
                bundle.putStringArrayList("strings", baitsUsedStrsArray);
                bundle.putString("chartName", getString(R.string.chart_baits_used));

                navController.navigate(R.id.nav_pie_chart, bundle);
            }
        });
    }

    private void getCatchesByLocation(View root, String query){
        //Catches By Location
        Map<String, Integer> catchesByLocation = new HashMap<String, Integer>();

        if(query == getString(R.string.statistics_no_fisherman)){
            for(Catch c : catchesList){
                for(Location l : locationsList){

                    Trip trip = null;
                    for (Trip t : tripsList){
                        if (t.ID == c.trip){
                            trip = t;
                        }
                    }

                    if (l.ID == locationsList.get(trip.location).ID){
                        if (catchesByLocation.containsKey(l.name)){
                            catchesByLocation.put(l.name, catchesByLocation.get(l.name) + 1);
                            continue;
                        } else {
                            catchesByLocation.put(l.name, 1);
                            continue;
                        }
                    }
                }
            }
        } else {
            Log.d("test", "getCatchesByLocation: " + query);

            Fisherman fisherman = null;
            for (Fisherman f : fishermenList){
                if ((f.firstName + " " + f.lastName).equals(query)){
                    fisherman = f;
                    break;
                }
            }

            for(Catch c : catchesList){
                for(Location l  : locationsList){

                    Trip trip = null;
                    for (Trip t : tripsList){
                        if (t.ID == c.trip){
                            trip = t;
                        }
                    }

                    if ((l.ID == locationsList.get(trip.location).ID) && (fisherman.ID == c.fisherman)){
                        if (catchesByLocation.containsKey(l.name)){
                            catchesByLocation.put(l.name, catchesByLocation.get(l.name) + 1);
                            continue;
                        } else {
                            catchesByLocation.put(l.name, 1);
                            continue;
                        }
                    }
                }
            }
        }

        LinkedHashMap<String, Integer> catchesByLocationSorted = new LinkedHashMap<>();

        //Use Comparator.reverseOrder() for reverse ordering
        catchesByLocation.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> catchesByLocationSorted.put(x.getKey(), x.getValue()));

        List<String> catchesByLocationstrings = new ArrayList<String>(catchesByLocationSorted.keySet());
        List<Integer> catchesByLocationintegers = new ArrayList<Integer>(catchesByLocationSorted.values());

        TextView firstLocationName = root.findViewById(R.id.statistics_catches_by_location_1_name);
        TextView firstLocationQuantity = root.findViewById(R.id.statistics_catches_by_location_1_quantity);
        if (1 > catchesByLocationstrings.size()){
            firstLocationName.setVisibility(View.GONE);
            firstLocationQuantity.setVisibility(View.GONE);
        } else {
            firstLocationName.setVisibility(View.VISIBLE);
            firstLocationQuantity.setVisibility(View.VISIBLE);
            firstLocationName.setText(catchesByLocationstrings.get(0));
            firstLocationQuantity.setText(catchesByLocationintegers.get(0).toString());
        }

        TextView secondLocationName = root.findViewById(R.id.statistics_catches_by_location_2_name);
        TextView secondLocationQuantity = root.findViewById(R.id.statistics_catches_by_location_2_quantity);

        if (2 > catchesByLocationstrings.size()){
            secondLocationName.setVisibility(View.GONE);
            secondLocationQuantity.setVisibility(View.GONE);
        } else {
            secondLocationName.setVisibility(View.VISIBLE);
            secondLocationQuantity.setVisibility(View.VISIBLE);
            secondLocationName.setText(catchesByLocationstrings.get(1));
            secondLocationQuantity.setText(catchesByLocationintegers.get(1).toString());
        }

        TextView thirdLocationName = root.findViewById(R.id.statistics_catches_by_location_3_name);
        TextView thirdLocationQuantity = root.findViewById(R.id.statistics_catches_by_location_3_quantity);
        if (3 > catchesByLocationstrings.size()){
            thirdLocationName.setVisibility(View.GONE);
            thirdLocationQuantity.setVisibility(View.GONE);
        } else {
            thirdLocationName.setVisibility(View.VISIBLE);
            thirdLocationQuantity.setVisibility(View.VISIBLE);
            thirdLocationName.setText(catchesByLocationstrings.get(2));
            thirdLocationQuantity.setText(catchesByLocationintegers.get(2).toString());
        }

        Button showMoreLocationCatches = root.findViewById(R.id.button_statistics_show_more_catches_by_location);

        showMoreLocationCatches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

                Bundle bundle = new Bundle();

                ArrayList<Integer> locationCatchesIntsArray = new ArrayList<Integer>();
                locationCatchesIntsArray.addAll(catchesByLocationintegers);

                ArrayList<String> locationCatchesStrsArray = new ArrayList<String>();
                locationCatchesStrsArray.addAll(catchesByLocationstrings);

                bundle.putIntegerArrayList("values", locationCatchesIntsArray);
                bundle.putStringArrayList("strings", locationCatchesStrsArray);
                bundle.putString("chartName", getString(R.string.chart_catches_by_location));

                navController.navigate(R.id.nav_pie_chart, bundle);
            }
        });
    }

    private Bitmap loadImageFromStorage(String fileName, String directoryName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir(directoryName, Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private void getLongestCatch(View root, String query){
        Catch longestCatch = catchesList.get(0);

        if(query == getString(R.string.statistics_no_fisherman)){
            for(Catch c : catchesList){
                if (c.length > longestCatch.length){
                    longestCatch = c;
                }
            }
        } else {
            Log.d("test", "getLongestCatch: " + query);

            Fisherman fisherman = null;
            for (Fisherman f : fishermenList){
                if ((f.firstName + " " + f.lastName).equals(query)){
                    fisherman = f;
                    break;
                }
            }

            for(Catch c : catchesList){
                if ((c.fisherman == fisherman.ID) && (c.length > longestCatch.length)){
                    longestCatch = c;
                }
            }
        }

        TextView longestCatchSpeciesName = root.findViewById(R.id.statistics_longest_catch_species);
        TextView longestCatchDate = root.findViewById(R.id.statistics_longest_catch_date);
        TextView longestCatchText = root.findViewById(R.id.statistics_longest_catch_text);

        for (Species s : speciesList){
            if (longestCatch.species == s.ID){
                longestCatchSpeciesName.setText(s.name);
            }
        }

        for (Trip t : tripsList){
            if (longestCatch.trip == t.ID){
                longestCatchDate.setText(t.startDate);
            }
        }

        longestCatchText.setText(getString(R.string.statistics_longest_catch) + " " + longestCatch.length + " " +  getString(R.string.unit_centimeters));

        Button showMoreLongCatches = root.findViewById(R.id.button_statistics_show_more_long_catches);

        showMoreLongCatches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

                Bundle bundle = new Bundle();

                bundle.putString("listName", getString(R.string.list_longest_catches));

                navController.navigate(R.id.nav_statistics_list, bundle);
            }
        });
    }

    private void getHeaviestCatch(View root, String query){
        Catch heaviestCatch = catchesList.get(0);

        if(query == getString(R.string.statistics_no_fisherman)){
            for(Catch c : catchesList){
                if (c.weight > heaviestCatch.weight){
                    heaviestCatch = c;
                }
            }
        } else {
            Log.d("test", "getHeaviestCatch: " + query);

            Fisherman fisherman = null;
            for (Fisherman f : fishermenList){
                if ((f.firstName + " " + f.lastName).equals(query)){
                    fisherman = f;
                    break;
                }
            }

            for(Catch c : catchesList){
                if ((c.fisherman == fisherman.ID) && (c.weight > heaviestCatch.weight)){
                    heaviestCatch = c;
                }
            }
        }

        TextView heaviestCatchSpeciesName = root.findViewById(R.id.statistics_heaviest_catch_species);
        TextView heaviestCatchDate = root.findViewById(R.id.statistics_heaviest_catch_date);
        TextView heaviestCatchText = root.findViewById(R.id.statistics_heaviest_catch_text);

        for (Species s : speciesList){
            if (heaviestCatch.species == s.ID){
                heaviestCatchSpeciesName.setText(s.name);
            }
        }

        for (Trip t : tripsList){
            if (heaviestCatch.trip == t.ID){
                heaviestCatchDate.setText(t.startDate);
            }
        }

        heaviestCatchText.setText(getString(R.string.statistics_heaviest_catch) + " " + heaviestCatch.weight + " " +  getString(R.string.unit_kilograms));

        Button showMoreHeavyCatches = root.findViewById(R.id.button_statistics_show_more_heavy_catches);

        showMoreHeavyCatches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

                Bundle bundle = new Bundle();

                bundle.putString("listName", getString(R.string.list_heaviest_catches));

                navController.navigate(R.id.nav_statistics_list, bundle);
            }
        });
    }
}