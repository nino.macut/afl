package nino.macut.afl.ui.locations;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import nino.macut.afl.MainActivity;
import nino.macut.afl.R;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Location;
import nino.macut.afl.db.AppDatabase;

public class AddMapToLocationFragment extends Fragment {

    SearchView searchView;
    Button finishAdding;

    String image;
    String type;
    String name;
    String description;
    String latitude;
    String longitude;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_add_map_to_location, container, false);

        searchView = root.findViewById(R.id.add_google_maps_search_bar);
        finishAdding = root.findViewById(R.id.button_finish_adding_location);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            image = bundle.getString("image");
            type = bundle.getString("type");
            name = bundle.getString("name");
            description = bundle.getString("description");
        }


        //Initialize map fragment
        SupportMapFragment supportMapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.add_google_maps_for_location);

        //Async map
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull GoogleMap googleMap) {
                //When map is loaded
                try{
                    boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.mapstyle));
                } catch (Resources.NotFoundException e){

                }

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        String location = searchView.getQuery().toString();
                        List<Address> addressList = null;

                        if (location != null || !location.equals("")){
                            Geocoder geocoder = new Geocoder(getContext());
                            try {
                                addressList = geocoder.getFromLocationName(location, 1);
                            } catch (IOException e){

                            }

                            Address address = addressList.get(0);
                            LatLng latLng = new LatLng(address.getLatitude(),address.getLongitude());
                            googleMap.addMarker(new MarkerOptions().position(latLng).title(location));
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));

                            latitude = String.valueOf(latLng.latitude);
                            longitude = String.valueOf(latLng.longitude);
                        }
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });

                finishAdding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (latitude != null && !latitude.isEmpty()) {
                            if (longitude != null && !longitude.isEmpty()) {
                                addNewLocation(image, name, type, description, latitude, longitude);

                                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                                navController.navigate(R.id.nav_locations);
                            }
                            else{
                                Toast.makeText(getActivity(), getString(R.string.toast_no_coordinates), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                        else{
                            Toast.makeText(getActivity(), getString(R.string.toast_no_coordinates), Toast.LENGTH_SHORT).show();
                            return;
                        }

                    }
                });


                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(@NonNull LatLng latLng) {
                        //When clicked on map
                        //Initialize marker options
                        MarkerOptions markerOptions = new MarkerOptions();
                        //Set position of marker
                        markerOptions.position(latLng);
                        //Set title of marker
                        markerOptions.title(latLng.latitude + " : " + latLng.longitude);
                        //Remove all markers
                        googleMap.clear();
                        //Zooming to marker
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                        googleMap.addMarker(markerOptions);

                        latitude = String.valueOf(latLng.latitude);
                        longitude = String.valueOf(latLng.longitude);
                    }
                });
            }
        });

        return root;
    }

    private void addNewLocation(String image, String name, String type, String description, String latitude, String longitude){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());

        Location location = new Location(image, name, type, description, latitude, longitude);
        db.locationDao().insertLocation(location);
    }
}