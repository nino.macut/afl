package nino.macut.afl.ui.statistics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.util.Range;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import de.hdodenhof.circleimageview.CircleImageView;
import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.DecimalRemover;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.Species;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.baits.BaitsFragment;

public class PieChartFragment extends Fragment implements OnChartValueSelectedListener {
    private PieChart pieChart;
    private String chartName;
    private String selectedItem = "none";
    private List<Trip> tripsList;
    private List<Bait> baitsList;
    private List<Species> speciesList;
    private List<Fisherman> fishermenList;
    private List<Catch> catchesList;
    private List<Location> locationsList;
    private View root;


    DecimalFormat df = new DecimalFormat("###,###,###");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_pie_chart, container, false);

        pieChart = root.findViewById(R.id.pie_chart);
        loadLists();



        Bundle bundle = this.getArguments();

        if(bundle != null){
            chartName = bundle.getString("chartName");
            ArrayList<Integer> values = bundle.getIntegerArrayList("values");
            ArrayList<String> strings = bundle.getStringArrayList("strings");

            ArrayList<PieEntry> entries = new ArrayList<>();

            IntStream.range(0, values.size()).forEachOrdered(n -> {
                entries.add(new PieEntry(values.get(n), strings.get(n)));
            });

            PieDataSet pieDataSet = new PieDataSet(entries, chartName);
            pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
            pieDataSet.setValueTextColor(Color.WHITE);
            pieDataSet.setValueFormatter(new DecimalRemover(new DecimalFormat("###,###,###")));
            pieDataSet.setValueTextSize(16f);
            pieDataSet.setSliceSpace(3f);

            PieData pieData = new PieData(pieDataSet);

            pieChart.setData(pieData);
            pieChart.setEntryLabelColor(Color.WHITE);
            pieChart.getDescription().setEnabled(false);
            pieChart.setCenterText(chartName);
            pieChart.setCenterTextColor(Color.WHITE);
            pieChart.setCenterTextSize(22f);
            pieChart.setHoleColor(Color.TRANSPARENT);
            pieChart.setDrawHoleEnabled(true);
            pieChart.setHoleRadius(60);
            pieChart.setTransparentCircleRadius(65);
            pieChart.setTransparentCircleAlpha(50);
            pieChart.getLegend().setEnabled(false);
            pieChart.animate();
            pieChart.setOnChartValueSelectedListener(this);

        }

        fillStatistics(root);
        return root;
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        tripsList = db.tripDao().getAllTrips();
        speciesList = db.speciesDao().getAllSpecies();
        baitsList = db.baitDao().getAllBaits();
        fishermenList = db.fishermanDao().getAllFishermen();
        catchesList = db.catchDao().getAllCatches();
        locationsList = db.locationDao().getAllLocations();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        PieEntry pe = (PieEntry) e;
        //Log.d("test", pe.getLabel());
        //Log.d("test", String.valueOf(pe.getValue()));

        RelativeSizeSpan largeSizeText = new RelativeSizeSpan(1f);

        RelativeSizeSpan mediumSizeText = new RelativeSizeSpan(.8f);

        RelativeSizeSpan smallSizeText = new RelativeSizeSpan(.5f);

        SpannableString spannableString = new SpannableString(pe.getLabel() + "\n" + df.format(pe.getValue()) + " " + getString(R.string.statistics_catches));
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.palette_darker_gray)), pe.getLabel().length(), spannableString.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableString.setSpan(largeSizeText, 0, pe.getLabel().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(mediumSizeText, pe.getLabel().length(), spannableString.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

        pieChart.setCenterText(spannableString);
        selectedItem = pe.getLabel();
        fillStatistics(root);
    }

    @Override
    public void onNothingSelected() {
        pieChart.setCenterText(chartName);
        selectedItem = "none";
        fillStatistics(root);
    }

    public void fillStatistics(View root){
        ImageView img1 = root.findViewById(R.id.pie_chart_statistics_icon1);
        ImageView img2 = root.findViewById(R.id.pie_chart_statistics_icon2);
        ImageView img3 = root.findViewById(R.id.pie_chart_statistics_icon3);

        TextView selectedText = root.findViewById(R.id.pie_chart_statistics_selection_name);

        TextView text1 = root.findViewById(R.id.pie_chart_statistics_part_3_text_1);
        TextView num1 = root.findViewById(R.id.pie_chart_statistics_part_3_num_1);
        TextView text2 = root.findViewById(R.id.pie_chart_statistics_part_3_text_2);
        TextView num2 = root.findViewById(R.id.pie_chart_statistics_part_3_num_2);

        ListView listView = root.findViewById(R.id.pie_chart_statistics_listview);

        if (chartName == getString(R.string.chart_baits_used)){
            img1.setImageDrawable(getResources().getDrawable(R.drawable.ic_baits_no_pic));
            img2.setImageDrawable(getResources().getDrawable(R.drawable.ic_species_no_pic));
            img3.setImageDrawable(getResources().getDrawable(R.drawable.ic_statistics_no_pic));

            if(selectedItem == "none"){
                selectedText.setText(getString(R.string.chart_all_baits));
            } else {
                selectedText.setText(selectedItem);
            }

            text1.setText(getString(R.string.chart_baits));
            num1.setText(String.valueOf(baitsList.size()));

            text2.setText(getString(R.string.chart_catches));
            num2.setText(String.valueOf(catchesList.size()));

            Bait bait = null;
            for (Bait b : baitsList){
                if (b.name.equals(selectedItem)){
                    bait = b;
                }
            }

            Log.d("test", selectedItem);
            if( bait != null){
                Log.d("test", bait.toString());
            }


            Map<String, Integer> speciesCaughtWithbait = new HashMap<String, Integer>();

            for(Catch c : catchesList){
                if(bait != null){
                    if (c.bait == bait.ID){
                        for(Species s : speciesList){
                            if (s.ID == c.species){
                                if(speciesCaughtWithbait.containsKey(s.name)){
                                    speciesCaughtWithbait.put(s.name, speciesCaughtWithbait.get(s.name) + 1);
                                    continue;
                                } else {
                                    speciesCaughtWithbait.put(s.name, 1);
                                    continue;
                                }
                            }
                        }
                    }
                } else {
                    for(Species s : speciesList){
                        if (s.ID == c.species){
                            if(speciesCaughtWithbait.containsKey(s.name)){
                                speciesCaughtWithbait.put(s.name, speciesCaughtWithbait.get(s.name) + 1);
                                continue;
                            } else {
                                speciesCaughtWithbait.put(s.name, 1);
                                continue;
                            }
                        }
                    }
                }

            }

            LinkedHashMap<String, Integer> speciesCaughtWithBaitSorted = new LinkedHashMap<>();

            //Use Comparator.reverseOrder() for reverse ordering
            speciesCaughtWithbait.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .forEachOrdered(x -> speciesCaughtWithBaitSorted.put(x.getKey(), x.getValue()));

            List<String> speciesCaughtWithBaitstrings = new ArrayList<String>(speciesCaughtWithBaitSorted.keySet());
            List<Integer> speciesCaughtWithBaitintegers = new ArrayList<Integer>(speciesCaughtWithBaitSorted.values());

            loadList(listView, speciesCaughtWithBaitstrings, speciesCaughtWithBaitintegers);
        }
        else if (chartName == getString(R.string.chart_catches_by_location)){
            img1.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_no_pic));
            img2.setImageDrawable(getResources().getDrawable(R.drawable.ic_species_no_pic));
            img3.setImageDrawable(getResources().getDrawable(R.drawable.ic_statistics_no_pic));

            if(selectedItem == "none"){
                selectedText.setText(getString(R.string.chart_all_locations));
            } else {
                selectedText.setText(selectedItem);
            }

            text1.setText(getString(R.string.chart_locations));
            num1.setText(String.valueOf(locationsList.size()));

            text2.setText(getString(R.string.chart_catches));
            num2.setText(String.valueOf(catchesList.size()));

            Location location = null;
            for (Location l : locationsList){
                if (l.name.equals(selectedItem)){
                    location = l;
                }
            }

            Log.d("test", selectedItem);
            if( location != null){
                Log.d("test", location.toString());
            }


            Map<String, Integer> speciesCaughtBylocation = new HashMap<String, Integer>();

            for(Catch c : catchesList){
                if(location != null){
                    for (Trip t : tripsList){
                        if (locationsList.get(t.location).name.equals(selectedItem)){
                            if (t.ID == c.trip){
                                for(Species s : speciesList){
                                    if (s.ID == c.species){
                                        if(speciesCaughtBylocation.containsKey(s.name)){
                                            speciesCaughtBylocation.put(s.name, speciesCaughtBylocation.get(s.name) + 1);
                                            continue;
                                        } else {
                                            speciesCaughtBylocation.put(s.name, 1);
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    for(Species s : speciesList){
                        if (s.ID == c.species){
                            if(speciesCaughtBylocation.containsKey(s.name)){
                                speciesCaughtBylocation.put(s.name, speciesCaughtBylocation.get(s.name) + 1);
                                continue;
                            } else {
                                speciesCaughtBylocation.put(s.name, 1);
                                continue;
                            }
                        }
                    }
                }

            }

            LinkedHashMap<String, Integer> speciesCaughtByLocationSorted = new LinkedHashMap<>();

            //Use Comparator.reverseOrder() for reverse ordering
            speciesCaughtBylocation.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .forEachOrdered(x -> speciesCaughtByLocationSorted.put(x.getKey(), x.getValue()));

            List<String> speciesCaughtByLocationstrings = new ArrayList<String>(speciesCaughtByLocationSorted.keySet());
            List<Integer> speciesCaughtByLocationintegers = new ArrayList<Integer>(speciesCaughtByLocationSorted.values());

            loadList(listView, speciesCaughtByLocationstrings, speciesCaughtByLocationintegers);
        } else if (chartName == getString(R.string.chart_catches_by_species)){
            img1.setImageDrawable(getResources().getDrawable(R.drawable.ic_species_no_pic));
            img2.setImageDrawable(getResources().getDrawable(R.drawable.ic_baits_no_pic));
            img3.setImageDrawable(getResources().getDrawable(R.drawable.ic_statistics_no_pic));

            if(selectedItem == "none"){
                selectedText.setText(getString(R.string.chart_all_species));
            } else {
                selectedText.setText(selectedItem);
            }

            text1.setText(getString(R.string.chart_species));
            num1.setText(String.valueOf(speciesList.size()));

            text2.setText(getString(R.string.chart_catches));
            num2.setText(String.valueOf(catchesList.size()));

            Species species = null;
            for (Species s : speciesList){
                if (s.name.equals(selectedItem)){
                    species = s;
                }
            }

            Log.d("test", selectedItem);
            if( species != null){
                Log.d("test", species.toString());
            }


            Map<String, Integer> speciesCaught = new HashMap<String, Integer>();

            for(Catch c : catchesList){
                if(species != null){
                    if(c.species == species.ID){
                        for (Bait b : baitsList){
                            if (c.bait == b.ID){
                                if(speciesCaught.containsKey(b.name)){
                                    speciesCaught.put(b.name, speciesCaught.get(b.name) + 1);
                                    continue;
                                } else {
                                    speciesCaught.put(b.name, 1);
                                    continue;
                                }
                            }
                        }
                    }
                } else {
                    for(Bait b : baitsList){
                        if (b.ID == c.bait){
                            if(speciesCaught.containsKey(b.name)){
                                speciesCaught.put(b.name, speciesCaught.get(b.name) + 1);
                                continue;
                            } else {
                                speciesCaught.put(b.name, 1);
                                continue;
                            }
                        }
                    }
                }
            }

            LinkedHashMap<String, Integer> speciesCaughtSorted = new LinkedHashMap<>();

            //Use Comparator.reverseOrder() for reverse ordering
            speciesCaught.entrySet()
                    .stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .forEachOrdered(x -> speciesCaughtSorted.put(x.getKey(), x.getValue()));

            List<String> speciesCaughtstrings = new ArrayList<String>(speciesCaughtSorted.keySet());
            List<Integer> speciesCaughtintegers = new ArrayList<Integer>(speciesCaughtSorted.values());

            loadList(listView, speciesCaughtstrings, speciesCaughtintegers);
        }
    }

    private void loadList(ListView list, List<String> strings, List<Integer> integers){

            List<ArrayItem> arrayItems = new ArrayList();
            int i = 0;
            for (String s : strings){
                ArrayItem ai = new ArrayItem(strings.get(i), integers.get(i));
                arrayItems.add(ai);
                i++;
            }

            ChartListAdapter speciesCaughtWithBaitAdapter = new ChartListAdapter(getActivity(), arrayItems);
            list.setAdapter(speciesCaughtWithBaitAdapter);

            int itemcount = speciesCaughtWithBaitAdapter.getCount();
            ViewGroup.LayoutParams params = list.getLayoutParams();
            params.height = (itemcount * 83); //Dinamically change listview height
            list.setLayoutParams(params);
            list.requestLayout();

    }

    private class ArrayItem {
        public String text;
        public Integer number;

        public ArrayItem(String text, Integer number){
            this.text = text;
            this.number = number;
        }
    }

    public class ChartListAdapter extends ArrayAdapter<ArrayItem> {
        public ChartListAdapter(Context context, List<ArrayItem> itemsList){
            super(context, R.layout.listitem_chartitem, itemsList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            ArrayItem arrayItem = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_chartitem,parent,false);
            }

            Log.d("test", arrayItem.text);
            Log.d("test", String.valueOf(arrayItem.number));
            TextView text = convertView.findViewById(R.id.chart_listitem_text);
            TextView number = convertView.findViewById(R.id.chart_listitem_number);

            text.setText(arrayItem.text);
            number.setText(String.valueOf(arrayItem.number));

            return convertView;
        }
    }
}