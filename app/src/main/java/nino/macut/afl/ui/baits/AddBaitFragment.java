package nino.macut.afl.ui.baits;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.bait_categories.AddBaitCategoryDialogFragment;
import nino.macut.afl.ui.bait_categories.BaitCategoriesDialogFragment;

public class AddBaitFragment extends Fragment {

    private ImageView previewImage;
    private Button buttonUploadBaitImage;
    private Button buttonAddNewBait;
    private Button buttonAddNewCategory;
    private String fileName;
    Bundle inputBundle;

    private List<BaitCategory> baitCategoriesList;

    int SELECT_IMAGE_CODE = 1;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_bait, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadList();
        fillBaitCategoriesDropdown(root);
        fillTypeDropdown(root);

        inputBundle = this.getArguments();

        if(inputBundle != null){
            fileName = inputBundle.getString("fileName");
            String nameString = inputBundle.getString("nameString");
            String descriptionString = inputBundle.getString("descriptionString");
            String typeString = inputBundle.getString("typeString");
            String brandString = inputBundle.getString("brandString");
            String colorString = inputBundle.getString("colorString");
            Float sizeFloat = inputBundle.getFloat("sizeFloat");

            EditText name = root.findViewById(R.id.text_bait_name_input);
            name.setText(nameString);

            EditText description = root.findViewById(R.id.text_bait_description_input);
            description.setText(descriptionString);

            Spinner categoryDropdown = root.findViewById(R.id.dropdown_bait_bait_category);
            categoryDropdown.setSelection(0);

            Spinner typeDropdown = root.findViewById(R.id.dropdown_bait_type);
            typeDropdown.setSelection(((ArrayAdapter)typeDropdown.getAdapter()).getPosition(typeString));


            EditText color = root.findViewById(R.id.text_bait_color_input);
            color.setText(colorString);

            EditText brand = root.findViewById(R.id.text_bait_brand_input);
            brand.setText(brandString);

            EditText size = root.findViewById(R.id.text_bait_brand_input);
            if (sizeFloat == 0){
                size.setText("");
            } else {
                size.setText(sizeFloat.toString());
            }
        }

        //Odabir profilne slike.
        previewImage = root.findViewById(R.id.image_bait_preview);
        buttonUploadBaitImage = root.findViewById(R.id.button_upload_bait_image);

        buttonUploadBaitImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Title"), SELECT_IMAGE_CODE);
            }
        });

        buttonAddNewCategory = root.findViewById(R.id.button_add_new_bait_category);

        buttonAddNewCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddBaitCategoryDialogFragment df = new AddBaitCategoryDialogFragment();
                df.show(getChildFragmentManager(), "AddBaitCategoryDialogFragment");
            }
        });

        Spinner baitcategory = root.findViewById(R.id.dropdown_bait_bait_category);

        baitcategory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadList();
                    fillBaitCategoriesDropdown(root);
                    return false;
                }
                return false;
            }
        });

        //Kreiranje i unos lokacije u text file.
        buttonAddNewBait = root.findViewById(R.id.button_add_new_bait);

        buttonAddNewBait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dohvati podatke.
                Bitmap bitmap;
                Boolean hasPicture = false;
                try{
                    BitmapDrawable image = (BitmapDrawable)previewImage.getDrawable();
                    bitmap = image.getBitmap();
                    hasPicture = true;
                }
                catch (Exception e){
                    e.printStackTrace();
                    bitmap = null;
                    hasPicture = false;
                }

                int categoryID;
                Spinner category = root.findViewById(R.id.dropdown_bait_bait_category);
                if (category.getSelectedItem() != null) {
                    categoryID = category.getSelectedItemPosition();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_category), Toast.LENGTH_SHORT).show();
                    return;
                }

                EditText name = root.findViewById(R.id.text_bait_name_input);
                String nameString = name.getText().toString();

                EditText description = root.findViewById(R.id.text_bait_description_input);
                String descriptionString = description.getText().toString();

                Spinner typeDropdown = root.findViewById(R.id.dropdown_bait_type);
                String typeString = typeDropdown.getSelectedItem().toString();

                EditText color = root.findViewById(R.id.text_bait_color_input);
                String colorString = color.getText().toString();

                EditText brand = root.findViewById(R.id.text_bait_brand_input);
                String brandString = brand.getText().toString();

                EditText size = root.findViewById(R.id.text_bait_size_input);
                float sizeFloat;
                if (!size.getText().toString().isEmpty()){
                    sizeFloat = Float.parseFloat(size.getText().toString());
                } else {
                    sizeFloat = 0;
                }


                if (TextUtils.isEmpty(name.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_bait_name), Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(color.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_bait_color), Toast.LENGTH_SHORT).show();
                    return;
                }

                //Spremi sliku u internu memoriju.
                String fileName = "bait" + nameString + ".jpg";

                if(hasPicture == true){
                    saveToInternalStorage(bitmap, fileName);
                }
                else if (hasPicture == false){
                    fileName = "nofile";
                }

                addNewBait(fileName, nameString, brandString, categoryID, typeString, sizeFloat, colorString, descriptionString);

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_baits);
            }
        });
        return root;
    }

    private void addNewBait(String fileName, String name, String brand, int categoryID, String type, float size, String color, String description){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());

        Bait bait = new Bait(fileName, name, brand, categoryID, type, size, color, description);
        db.baitDao().insertBait(bait);
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        baitCategoriesList = db.baitCategoryDao().getAllBaitCategories();
    }

    private void fillTypeDropdown(View root){
        Spinner typeDropdown = root.findViewById(R.id.dropdown_bait_type);
        String[] items = getActivity().getResources().getStringArray(R.array.array_types_of_baits);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        typeDropdown.setAdapter(adapter);
    }

    private void fillBaitCategoriesDropdown(View root){
        Spinner baitCategoriesDropdown = root.findViewById(R.id.dropdown_bait_bait_category);
        String[] items = new String[baitCategoriesList.size()];

        int i = 0;
        for (BaitCategory b: baitCategoriesList
        ) {
            items[i] = b.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        baitCategoriesDropdown.setAdapter(adapter);
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String fileName){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Baits", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    //Handler upita.
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_IMAGE_CODE){
            Uri uri = data.getData();
            previewImage.setImageURI(uri);
        }
    }
}