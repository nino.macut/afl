package nino.macut.afl.ui.locations;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import nino.macut.afl.MainActivity;
import nino.macut.afl.R;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Location;
import nino.macut.afl.db.AppDatabase;


public class EditLocationFragment extends Fragment {

    private ImageView previewImage;
    private Button buttonUploadLocationImage;
    private Button buttonEditLocation;
    private Button buttonDeleteLocation;
    private List<Location> locationsList;
    private int editID;

    int SELECT_IMAGE_CODE = 1;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static EditLocationFragment newInstance() {
        return new EditLocationFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_edit_location, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadLocationsList();
        fillTypeDropdown(root);

        previewImage = root.findViewById(R.id.image_location_preview);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            editID = bundle.getInt("editID");
            Location location = locationsList.get(editID);
            Bitmap bitmap = loadImageFromStorage(location.image);
            previewImage.setImageBitmap(bitmap);

            EditText name = root.findViewById(R.id.text_location_name_input);
            name.setText(location.name);
            EditText description = root.findViewById(R.id.text_location_description_input);
            description.setText(location.description);
            Spinner typeDropdown = root.findViewById(R.id.dropdown_location_type);
            typeDropdown.setSelection(((ArrayAdapter)typeDropdown.getAdapter()).getPosition(location.type));
        }

        //Odabir profilne slike.
        buttonUploadLocationImage = root.findViewById(R.id.button_upload_location_image);

        buttonUploadLocationImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Title"), SELECT_IMAGE_CODE);
            }
        });

        //Brisanje lokacije
        buttonDeleteLocation = root.findViewById(R.id.button_delete_location);

        buttonDeleteLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle(R.string.menu_confirm);
                builder.setMessage(R.string.message_are_you_sure_delete);

                builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Location location = locationsList.get(editID);
                        removeLocationFromDB(location);
                        dialog.dismiss();
                        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                        navController.navigate(R.id.nav_locations);
                    }
                });

                builder.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        //Kreiranje i unos lokacije u text file.
        buttonEditLocation = root.findViewById(R.id.button_edit_location);

        buttonEditLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dohvati podatke.
                Bitmap bitmap;
                Boolean hasPicture = false;
                try{
                    BitmapDrawable image = (BitmapDrawable)previewImage.getDrawable();
                    bitmap = image.getBitmap();
                    hasPicture = true;
                }
                catch (Exception e){
                    e.printStackTrace();
                    bitmap = null;
                    hasPicture = false;
                }

                EditText name = root.findViewById(R.id.text_location_name_input);
                String nameString = name.getText().toString();
                EditText description = root.findViewById(R.id.text_location_description_input);
                String descriptionString = description.getText().toString();
                Spinner typeDropdown = root.findViewById(R.id.dropdown_location_type);
                String typeString = typeDropdown.getSelectedItem().toString();

                if (TextUtils.isEmpty(name.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_location_name), Toast.LENGTH_SHORT).show();
                    return;
                }

                //Spremi sliku u internu memoriju.
                String fileName = "location" + nameString + ".jpg";

                if(hasPicture == true){
                    saveToInternalStorage(bitmap, fileName);
                }
                else if (hasPicture == false){
                    fileName = "nofile";
                }



                Bundle bndl = new Bundle();
                bndl.putInt("editID", editID);
                bndl.putString("image", fileName);
                bndl.putString("type", typeString);
                bndl.putString("name", nameString);
                bndl.putString("description", descriptionString);

                /*
                EditMapOfLocationFragment fragment = new EditMapOfLocationFragment();
                fragment.setArguments(bndl);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,fragment).commit();

                 */


                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_edit_map_of_location, bndl);
                 
            }
        });
        return root;
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Locations", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadLocationsList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        locationsList = db.locationDao().getAllLocations();
    }

    private void editLocationInDB(Location location){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.locationDao().updateLocation(location);
    }

    private void removeLocationFromDB(Location location){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.locationDao().deleteLocation(location);
    }

    private void fillTypeDropdown(View root){
        Spinner typeDropdown = root.findViewById(R.id.dropdown_location_type);
        String[] items = getActivity().getResources().getStringArray(R.array.array_types_of_water);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        typeDropdown.setAdapter(adapter);
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String fileName){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Locations", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    //Handler upita.
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_IMAGE_CODE){
            Uri uri = data.getData();
            previewImage.setImageURI(uri);
        }
    }
}