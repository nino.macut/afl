package nino.macut.afl.ui.statistics;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.DecimalRemover;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.Species;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.trips.TripsFragment;

public class ListFragment extends Fragment {

    ListView list;
    int selection;

    private List<Trip> tripsList;
    private List<Bait> baitsList;
    private List<Species> speciesList;
    private List<Fisherman> fishermenList;
    private List<Catch> catchesList;
    private List<Location> locationsList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);

        list = root.findViewById(R.id.list_statistics);
        loadLists();

        Bundle bundle = this.getArguments();

        if(bundle != null){
            TextView title = root.findViewById(R.id.text_list_title);
            TextView subtitle = root.findViewById(R.id.text_list_subtitle);

            String listName = bundle.getString("listName");
            if (listName.equals(getString(R.string.list_heaviest_catches))){
                title.setText(R.string.list_heaviest_catches);
                subtitle.setText(R.string.list_heaviest_catches_subtitle);
                selection = 1;
                loadList(selection);
            } else if (listName.equals(getString(R.string.list_longest_catches))){
                title.setText(R.string.list_longest_catches);
                subtitle.setText(R.string.list_longest_catches_subtitle);
                selection = 2;
                loadList(selection);
            }

        }

        return root;
    }

    private void loadList(int selection){
        if (selection == 1){
            catchesList.sort(Comparator.comparing(Catch::getWeight).reversed());
        } else if (selection == 2){
            catchesList.sort(Comparator.comparing(Catch::getLength).reversed());
        }
        CatchStatisticsListAdapter tripsAdapter = new CatchStatisticsListAdapter(getActivity(), catchesList);
        list.setAdapter(tripsAdapter);
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        tripsList = db.tripDao().getAllTrips();
        speciesList = db.speciesDao().getAllSpecies();
        baitsList = db.baitDao().getAllBaits();
        fishermenList = db.fishermanDao().getAllFishermen();
        catchesList = db.catchDao().getAllCatches();
        locationsList = db.locationDao().getAllLocations();
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir("Catches", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public class CatchStatisticsListAdapter extends ArrayAdapter<Catch> {
        public CatchStatisticsListAdapter(Context context, List<Catch> catchesList){
            super(context, R.layout.listitem_catch, catchesList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Catch catchy = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_catch,parent,false);
            }

            ImageView image = convertView.findViewById(R.id.image_catch_image_listitem);
            TextView species = convertView.findViewById(R.id.text_catch_species_listitem);
            TextView dateAndTime = convertView.findViewById(R.id.text_catch_date_listitem);
            TextView location = convertView.findViewById(R.id.text_catch_location_listitem);

            if (!catchy.image.equals("nofile")){
                Bitmap bitmap = loadImageFromStorage(catchy.image);
                image.setImageBitmap(bitmap);
            }
            else if (catchy.image.equals("nofile")){
                image.setImageResource(R.drawable.ic_catches_no_pic);
            }

            AppDatabase db = AppDatabase.getDbInstance(getActivity());
            List<Species> speciesList = db.speciesDao().getAllSpecies();
            List<Trip> tripsList = db.tripDao().getAllTrips();

            for(Species s : speciesList){
                if (s.ID == catchy.species){
                    species.setText(s.name);
                }
            }

            for(Trip t : tripsList){
                if (t.ID == catchy.trip){
                    dateAndTime.setText(t.startDate);
                }
            }

            if (selection == 1){
                location.setText(catchy.weight + getString(R.string.unit_kilograms));
            } else if (selection == 2){
                location.setText(catchy.length + getString(R.string.unit_centimeters));
            }

            return convertView;
        }
    }
}