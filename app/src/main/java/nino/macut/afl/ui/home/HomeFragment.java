package nino.macut.afl.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import nino.macut.afl.MainActivity;
import nino.macut.afl.R;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Species;
import nino.macut.afl.db.AppDatabase;

public class HomeFragment extends Fragment implements LocationListener {

    private final String url = "http://api.openweathermap.org/data/2.5/weather";
    private final String appid = "2c4ff5d50a3a2378fc0ae481ebc36012";
    DecimalFormat df = new DecimalFormat("#.##");

    TextView cityCountry;
    TextView skyConditions;
    TextView temperature;
    TextView pressure;
    TextView humidity;
    TextView wind;
    TextView suggestion;

    String city;
    String country;
    String skyCondition;
    double temp;
    float pres;
    float hum;
    float windS;

    List<Catch> catchesList;
    List<Species> speciesList;

    LocationManager locationManager;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        //Runtime permissions
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, 100);
        }

        suggestion = root.findViewById(R.id.home_species_suggestion);
        suggestion.setVisibility(View.GONE);

        setDefaults(root);
        getLocation();

        return root;
    }

    public void setDefaults(View root){
        cityCountry = root.findViewById(R.id.home_city_country);
        skyConditions = root.findViewById(R.id.home_sky_conditions);
        temperature = root.findViewById(R.id.home_temperature);
        pressure = root.findViewById(R.id.home_atmospheric_pressure);
        humidity = root.findViewById(R.id.home_humidity);
        wind = root.findViewById(R.id.home_wind_speed);

        cityCountry.setText(getString(R.string.home_city_country));
        skyConditions.setText(getString(R.string.home_unknown));
        temperature.setText(getString(R.string.home_unknown));
        pressure.setText(getString(R.string.home_unknown));
        humidity.setText(getString(R.string.home_unknown));
        wind.setText(getString(R.string.home_unknown));
    }

    @SuppressLint("MissingPermission")
    public void getLocation(){
        try {
            locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getWeatherDetails(Double lat, Double lng){
        String tempUrl = "";

        tempUrl = url + "?lat=" + lat + "&lon=" + lng + "&appid=" + appid;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, tempUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", response);
                String output = "";
                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    JSONArray jsonArray = jsonResponse.getJSONArray("weather");
                    JSONObject jsonObjectWeather = jsonArray.getJSONObject(0);

                    JSONObject jsonObjectMain = jsonResponse.getJSONObject("main");

                    temp = jsonObjectMain.getDouble("temp") - 273.15;
                    temperature.setText(df.format(temp) + " " + getString(R.string.unit_temperature));

                    pres = jsonObjectMain.getInt("pressure");
                    pressure.setText(pres + " " + getString(R.string.unit_pressure));
                    hum = jsonObjectMain.getInt("humidity"); //percentage
                    humidity.setText(hum + getString(R.string.unit_percentage));

                    JSONObject jsonObjectWind = jsonResponse.getJSONObject("wind");
                    String wnd = jsonObjectWind.getString("speed"); //m/s
                    windS = Float.parseFloat(wnd);
                    double ws = windS * 3.6;
                    wind.setText(df.format(ws) + " " + getString(R.string.unit_speed));

                    JSONObject jsonObjectClouds = jsonResponse.getJSONObject("clouds");
                    String clouds = jsonObjectClouds.getString("all"); //cloudyness in percentage
                    int cld = Integer.parseInt(clouds);
                    Context context = getContext();
                    String[] stringArray = context.getResources().getStringArray(R.array.array_sky_conditions);

                    if (cld >= 0 && cld <= 20){
                        skyConditions.setText(stringArray[0]);
                        skyCondition = stringArray[0];
                    } else if (cld > 20 && cld <= 40) {
                        skyConditions.setText(stringArray[1]);
                        skyCondition = stringArray[1];
                    } else if (cld > 40 && cld <= 60) {
                        skyConditions.setText(stringArray[2]);
                        skyCondition = stringArray[2];
                    } else if (cld > 60 && cld <= 80) {
                        skyConditions.setText(stringArray[3]);
                        skyCondition = stringArray[3];
                    } else {
                        skyConditions.setText(stringArray[4]);
                        skyCondition = stringArray[4];
                    }

                    JSONObject jsonObjectSys = jsonResponse.getJSONObject("sys");
                    country = jsonObjectSys.getString("country");
                    city = jsonResponse.getString("name");

                    cityCountry.setText(city + ", " + country);

                    loadLists();
                    calculateAverages();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), error.toString().trim(), Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        Log.d("response", "test");
        requestQueue.add(stringRequest);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();

        Log.d("response", String.valueOf(lat));
        Log.d("response", String.valueOf(lng));

        getWeatherDetails(lat, lng);
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

    public void calculateAverages(){
        List<SpeciesAverage> speciesAveragesList = new ArrayList<>();

        for(Species species : speciesList){
            SpeciesAverage speciesAverage = new SpeciesAverage(species.name);
            speciesAveragesList.add(speciesAverage);
        }

        for(SpeciesAverage speciesAverage : speciesAveragesList){
            int sID = 0;
            for (Species s : speciesList){
                if (s.name.equals(speciesAverage.speciesName)){
                    sID = s.ID;
                }
            }
            if (sID != 0){
                for (Catch c : catchesList){
                    if (c.species == sID){
                        speciesAverage.skyConditionValuesList.add(c.skyConditions);
                        speciesAverage.temperatureValuesList.add(c.temperature);
                        speciesAverage.pressureValuesList.add(c.atmosphericPressure);
                        speciesAverage.windValuesList.add(c.wind);
                        continue;
                    }
                }


                speciesAverage.skyConditionAverage = speciesAverage.calculateAverageFromListString(speciesAverage.skyConditionValuesList);
                speciesAverage.temperatureAverage = speciesAverage.calculateAverageFromListFloat(speciesAverage.temperatureValuesList);
                speciesAverage.windAverage = speciesAverage.calculateAverageFromListFloat(speciesAverage.windValuesList);
                speciesAverage.pressureAverage = speciesAverage.calculateAverageFromListFloat(speciesAverage.pressureValuesList);
            }
        }

        for(SpeciesAverage speciesAverage : speciesAveragesList){

            if(speciesAverage.skyConditionAverage.equals(skyCondition)){
                speciesAverage.points += 3;
            }

            if(speciesAverage.temperatureAverage > temp){
                if ((speciesAverage.temperatureAverage - temp) < 1){
                    speciesAverage.points += 3;
                }
                else if ((speciesAverage.temperatureAverage - temp) < 2){
                    speciesAverage.points += 2;
                }
                else if ((speciesAverage.temperatureAverage - temp) < 3){
                    speciesAverage.points += 1;
                }
            } else if (speciesAverage.temperatureAverage < temp){
                if ((temp - speciesAverage.temperatureAverage) < 1){
                    speciesAverage.points += 3;
                }
                else if ((temp - speciesAverage.temperatureAverage) < 2){
                    speciesAverage.points += 2;
                }
                else if ((temp - speciesAverage.temperatureAverage) < 3){
                    speciesAverage.points += 1;
                }
            } else if (speciesAverage.temperatureAverage == temp){
                speciesAverage.points += 3;
            }

            if(speciesAverage.pressureAverage > pres){
                if ((speciesAverage.pressureAverage - pres) < 1){
                    speciesAverage.points += 3;
                }
                else if ((speciesAverage.pressureAverage - pres) < 2){
                    speciesAverage.points += 2;
                }
                else if ((speciesAverage.pressureAverage - pres) < 3){
                    speciesAverage.points += 1;
                }
            } else if (speciesAverage.pressureAverage < pres){
                if ((pres - speciesAverage.pressureAverage) < 1){
                    speciesAverage.points += 3;
                }
                else if ((pres - speciesAverage.pressureAverage) < 2){
                    speciesAverage.points += 2;
                }
                else if ((pres - speciesAverage.pressureAverage) < 3){
                    speciesAverage.points += 1;
                }
            } else if (speciesAverage.pressureAverage == pres){
                speciesAverage.points += 3;
            }

            if(speciesAverage.windAverage > windS){
                if ((speciesAverage.windAverage - windS) < 1){
                    speciesAverage.points += 3;
                }
                else if ((speciesAverage.windAverage - windS) < 2){
                    speciesAverage.points += 2;
                }
                else if ((speciesAverage.windAverage - windS) < 3){
                    speciesAverage.points += 1;
                }
            } else if (speciesAverage.windAverage < windS){
                if ((windS - speciesAverage.windAverage) < 1){
                    speciesAverage.points += 3;
                }
                else if ((windS - speciesAverage.windAverage) < 2){
                    speciesAverage.points += 2;
                }
                else if ((windS - speciesAverage.windAverage) < 3){
                    speciesAverage.points += 1;
                }
            } else if (speciesAverage.windAverage == windS){
                speciesAverage.points += 3;
            }
        }

        for(SpeciesAverage s : speciesAveragesList){
            Log.d("test", s.speciesName.toString());
            Log.d("test", s.skyConditionAverage.toString());
            Log.d("test", s.temperatureAverage.toString());
            Log.d("test", s.windAverage.toString());
            Log.d("test", s.pressureAverage.toString());
            Log.d("test", String.valueOf(s.points));
        }

        int maxPoints = 0;
        String finalResult = "";
        List<String> listOfStrings = new ArrayList<>();

        for (SpeciesAverage speciesAverage : speciesAveragesList){
            if (speciesAverage.points > maxPoints){
                maxPoints = speciesAverage.points;
                finalResult = speciesAverage.speciesName;
                listOfStrings = new ArrayList<>();
                listOfStrings.add(finalResult);
            } else if (speciesAverage.points == maxPoints){
                finalResult = speciesAverage.speciesName;
                listOfStrings.add(finalResult);
            }
        }

        suggestion.setVisibility(View.VISIBLE);
        if (listOfStrings.size() > 1){
            suggestion.setText(getString(R.string.home_species_suggestion) + " ");
            int i = 0;
            for (String s : listOfStrings){
                if (i == 0){
                    suggestion.setText(suggestion.getText() + s);
                    i++;
                } else {
                    suggestion.setText(suggestion.getText() + ", " + s);
                    i++;
                }
            }
        } else {
            suggestion.setText(getString(R.string.home_species_suggestion) + " " + listOfStrings.get(0));
        }

    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        speciesList = db.speciesDao().getAllSpecies();
        catchesList = db.catchDao().getAllCatches();
    }

    public class SpeciesAverage {
        List<String> skyConditionValuesList;
        List<Float> temperatureValuesList;
        List<Float> pressureValuesList;
        List<Float> windValuesList;

        String skyConditionAverage;
        Float temperatureAverage;
        Float pressureAverage;
        Float windAverage;

        int points;

        String speciesName;

        public SpeciesAverage(String speciesName){
            this.speciesName = speciesName;

            skyConditionValuesList = new ArrayList<>();
            temperatureValuesList = new ArrayList<>();
            pressureValuesList = new ArrayList<>();
            windValuesList = new ArrayList<>();
            points = 0;
        }

        private String calculateAverageFromListString(List<String> list) {
            String result = "";
            int maxValue = 0;

            Map<String, Integer> stringsAndCounts = new HashMap<String, Integer>();

            if(!list.isEmpty()) {
                for (String s : list) {
                    if (stringsAndCounts.containsKey(s)){
                        stringsAndCounts.put(s, stringsAndCounts.get(s) + 1);
                        continue;
                    } else {
                        stringsAndCounts.put(s, 1);
                        continue;
                    }
                }

                LinkedHashMap<String, Integer> stringsAndCountsSorted = new LinkedHashMap<>();

                //Use Comparator.reverseOrder() for reverse ordering
                stringsAndCounts.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .forEachOrdered(x -> stringsAndCountsSorted.put(x.getKey(), x.getValue()));

                List<String> strings = new ArrayList<String>(stringsAndCountsSorted.keySet());
                List<Integer> counts = new ArrayList<Integer>(stringsAndCountsSorted.values());

                int i = 0;
                for (Integer num : counts){
                    if (num > maxValue){
                        maxValue = num;
                        result = strings.get(i);
                    }
                    i++;
                }

                return result;
            }
            return result;
        }

        private float calculateAverageFromListFloat(List<Float> list) {
            float sum = 0.0f;
            if(!list.isEmpty()) {
                for (Float f : list) {
                    sum += f;
                }
                return sum / list.size();
            }
            return sum;
        }
    }
}