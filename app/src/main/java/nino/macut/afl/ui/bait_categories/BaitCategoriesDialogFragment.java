package nino.macut.afl.ui.bait_categories;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.util.List;

import nino.macut.afl.MainActivity;
import nino.macut.afl.R;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.db.AppDatabase;

public class BaitCategoriesDialogFragment extends DialogFragment {

    private List<BaitCategory> baitCategoriesList;
    private ListView listOfBaitCategories;
    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private boolean addNew = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.dialog_fragment_bait_categories, container, false);

        verifyPermissions();
        loadBaitCategoriesList();
        initializeList(root);
        initializeAddButton(root);

        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadBaitCategoriesList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        baitCategoriesList = db.baitCategoryDao().getAllBaitCategories();
    }

    private void loadList(){
        BaitCategoriesDialogFragment.BaitCategoryAdapter baitCategoriesAdapter = new BaitCategoriesDialogFragment.BaitCategoryAdapter(getActivity(), baitCategoriesList);
        listOfBaitCategories.setAdapter(baitCategoriesAdapter);
    }

    private void initializeList(View root){
        listOfBaitCategories = root.findViewById(R.id.list_bait_categories);
        listOfBaitCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt("editID", position);
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_edit_bait_category, bundle);
            }
        });
        loadList();
    }

    private void initializeAddButton(View root){
        Button buttonAddBaitCategory = root.findViewById(R.id.button_add_bait_category);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        buttonAddBaitCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNew = true;
                AddBaitCategoryDialogFragment df = new AddBaitCategoryDialogFragment();
                df.show(getChildFragmentManager(), "AddBaitCategoryFragment");

                //navController.navigate(R.id.nav_add_bait_category);
            }
        });
    }

    public class BaitCategoryAdapter extends ArrayAdapter<BaitCategory> {
        public BaitCategoryAdapter(Context context, List<BaitCategory> baitCategoriesList){
            super(context, R.layout.listitem_bait_category, baitCategoriesList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            BaitCategory baitCategory = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_bait_category,parent,false);
            }

            TextView name = convertView.findViewById(R.id.text_bait_category_name_listitem);
            name.setText(baitCategory.name + "");

            return convertView;
        }
    }
}
