package nino.macut.afl.ui.species;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import nino.macut.afl.R;
import nino.macut.afl.classes.Species;
import nino.macut.afl.db.AppDatabase;

public class AddSpeciesDialogFragment extends DialogFragment {

    ImageView portrait;
    Button buttonUploadSpeciesPortrait;
    Button buttonAddNewSpecies;

    String FILENAME = "species";
    int SELECT_IMAGE_CODE = 1;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.dialog_fragment_add_species, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        fillTypeDropdown(root);

        //Odabir profilne slike.
        portrait = root.findViewById(R.id.image_species_image);
        buttonUploadSpeciesPortrait = root.findViewById(R.id.button_upload_species_image);

        buttonUploadSpeciesPortrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Title"), SELECT_IMAGE_CODE);
            }
        });

        //Kreiranje i unos fisherman-a u text file.
        buttonAddNewSpecies = root.findViewById(R.id.button_add_new_species);

        buttonAddNewSpecies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dohvati podatke.
                Bitmap bitmap;
                Boolean hasPicture = false;
                try{
                    BitmapDrawable image = (BitmapDrawable)portrait.getDrawable();
                    bitmap = image.getBitmap();
                    hasPicture = true;
                }
                catch (Exception e){
                    e.printStackTrace();
                    bitmap = null;
                    hasPicture = false;
                }

                EditText name = root.findViewById(R.id.text_species_name_input);
                String nameString = name.getText().toString();
                EditText description = root.findViewById(R.id.text_species_description_input);
                String descriptionString = description.getText().toString();

                Spinner typeDropdown = root.findViewById(R.id.dropdown_species_type);
                String typeString = typeDropdown.getSelectedItem().toString();

                if (TextUtils.isEmpty(name.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_species_name), Toast.LENGTH_SHORT).show();
                    return;
                }

                //Spremi sliku u internu memoriju.
                String fileName = "species" + nameString + ".jpg";

                if(hasPicture == true){
                    saveToInternalStorage(bitmap, fileName);
                }
                else if (hasPicture == false){
                    fileName = "nofile";
                }


                //Stvori Fisherman-a.
                addNewSpecies(fileName, nameString, typeString, descriptionString);

                dismiss();
            }
        });

        return root;
    }

    private void fillTypeDropdown(View root){
        Spinner typeDropdown = root.findViewById(R.id.dropdown_species_type);
        String[] items = getActivity().getResources().getStringArray(R.array.array_species_diets);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        typeDropdown.setAdapter(adapter);
    }

    private void addNewSpecies(String fileName, String name, String type, String description){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());

        Species species = new Species(fileName, name, type, description);
        db.speciesDao().insertSpecies(species);
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String fileName){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Species", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    //Handler upita.
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_IMAGE_CODE){
            Uri uri = data.getData();
            portrait.setImageURI(uri);
        }
    }
}
