package nino.macut.afl.ui.fishermen;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.db.AppDatabase;

public class EditFishermanFragment extends Fragment {

    private ImageView portrait;
    private Button buttonUploadFishermanPortrait;
    private EditText dateOfBirth;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private Button buttonEditFisherman;
    private Button buttonDeleteFisherman;
    private List<Fisherman> fishermanList;
    private int editID;

    int SELECT_IMAGE_CODE = 1;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static EditFishermanFragment newInstance() {
        return new EditFishermanFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_edit_fisherman, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadFishermenList();

        //Odabir profilne slike.
        portrait = root.findViewById(R.id.image_fisherman_portrait);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            editID = bundle.getInt("editID");
            Fisherman fisherman = fishermanList.get(editID);
            Bitmap bitmap = loadImageFromStorage(fisherman.image);
            portrait.setImageBitmap(bitmap);

            EditText firstName = root.findViewById(R.id.text_fisherman_first_name_input);
            firstName.setText(fisherman.firstName);
            EditText lastName = root.findViewById(R.id.text_fisherman_last_name_input);
            lastName.setText(fisherman.lastName);
            EditText dateOfBirth = root.findViewById(R.id.date_date_of_birth);
            dateOfBirth.setText(fisherman.dateOfBirth);
        }

        buttonUploadFishermanPortrait = root.findViewById(R.id.button_upload_fisherman_portrait);

        buttonUploadFishermanPortrait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Title"), SELECT_IMAGE_CODE);
            }
        });

        //Odabir datuma rođenja.
        dateOfBirth = root.findViewById(R.id.date_date_of_birth);

        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DateDialogTheme, dateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                dateOfBirth.setText("" + dayOfMonth + "/" + (month + 1) + "/" + year);
            }
        };

        //Brisanje ribiča
        buttonDeleteFisherman = root.findViewById(R.id.button_delete_fisherman);

        buttonDeleteFisherman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle(R.string.menu_confirm);
                builder.setMessage(R.string.message_are_you_sure_delete);

                builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Fisherman fisherman = fishermanList.get(editID);
                        removeFishermanFromDB(fisherman);
                        dialog.dismiss();
                        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                        navController.navigate(R.id.nav_fishermen);
                    }
                });

                builder.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        //Kreiranje i unos fisherman-a u text file.
        buttonEditFisherman = root.findViewById(R.id.button_edit_fisherman);

        buttonEditFisherman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dohvati podatke.
                Bitmap bitmap;
                Boolean hasPicture = false;
                try{
                    BitmapDrawable image = (BitmapDrawable)portrait.getDrawable();
                    bitmap = image.getBitmap();
                    hasPicture = true;
                }
                catch (Exception e){
                    e.printStackTrace();
                    bitmap = null;
                    hasPicture = false;
                }

                EditText dateOfBirth = root.findViewById(R.id.date_date_of_birth);
                String date = dateOfBirth.getText().toString();
                EditText firstName = root.findViewById(R.id.text_fisherman_first_name_input);
                String firstNameString = firstName.getText().toString();
                EditText lastName = root.findViewById(R.id.text_fisherman_last_name_input);
                String lastNameString = lastName.getText().toString();

                if (TextUtils.isEmpty(dateOfBirth.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_date_of_birth), Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (TextUtils.isEmpty(firstName.getText())) {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_firstName), Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (TextUtils.isEmpty(lastName.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_lastName), Toast.LENGTH_SHORT).show();
                    return;
                }


                //Spremi sliku u internu memoriju.
                String fileName = firstNameString + lastNameString + ".jpg";

                if(hasPicture == true){
                    saveToInternalStorage(bitmap, fileName);
                }
                else if (hasPicture == false){
                    fileName = "nofile";
                }


                //Stvori Fisherman-a.
                Fisherman fisherman = fishermanList.get(editID);

                fisherman.image = fileName;
                fisherman.dateOfBirth = date;
                fisherman.firstName = firstNameString;
                fisherman.lastName = lastNameString;

                editFishermanInDB(fisherman);

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_fishermen);
            }
        });
        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadFishermenList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        fishermanList = db.fishermanDao().getAllFishermen();
    }

    private void editFishermanInDB(Fisherman fisherman){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.fishermanDao().updateFisherman(fisherman);
    }

    private void removeFishermanFromDB(Fisherman fisherman){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.fishermanDao().deleteFisherman(fisherman);
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Fishermen", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String fileName){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Fishermen", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    //Handler upita.
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_IMAGE_CODE){
            Uri uri = data.getData();
            portrait.setImageURI(uri);
        }
    }
}