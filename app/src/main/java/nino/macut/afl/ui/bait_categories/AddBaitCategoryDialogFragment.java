package nino.macut.afl.ui.bait_categories;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.db.AppDatabase;

public class AddBaitCategoryDialogFragment extends DialogFragment {

    Button buttonAddNewBaitCategory;
    Bundle bundle;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.dialog_fragment_add_bait_category, container, false);

        verifyPermissions();

        bundle = this.getArguments();

        //Kreiranje i unos fisherman-a u text file.
        buttonAddNewBaitCategory = root.findViewById(R.id.button_add_new_bait_category);

        buttonAddNewBaitCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText name = root.findViewById(R.id.text_bait_category_name_input);
                String nameString = name.getText().toString();


                if (TextUtils.isEmpty(name.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_bait_category_name), Toast.LENGTH_SHORT).show();
                    return;
                }

                //Stvori Fisherman-a.
                addNewBaitCategory(nameString);
                dismiss();
            }
        });

        return root;
    }

    private void addNewBaitCategory(String name){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());

        BaitCategory baitCategory = new BaitCategory(name);
        db.baitCategoryDao().insertBaitCategory(baitCategory);
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }
}
