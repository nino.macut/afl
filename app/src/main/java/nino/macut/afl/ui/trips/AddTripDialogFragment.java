package nino.macut.afl.ui.trips;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.util.Calendar;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.catches.AddCatchFragment;

public class AddTripDialogFragment extends DialogFragment {

    private Button buttonAddNewTrip;
    private List<Trip> tripsList;
    private List<Location> locationsList;

    private DatePickerDialog.OnDateSetListener startDateSetListener;
    private DatePickerDialog.OnDateSetListener endDateSetListener;

    int SELECT_IMAGE_CODE = 1;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.dialog_fragment_add_trip, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadLists();
        fillLocationDropdown(root);

        //Odabir datuma početka.
        EditText startDate = root.findViewById(R.id.date_trip_start_date);

        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DateDialogTheme, startDateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        startDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                startDate.setText("" + dayOfMonth + "/" + (month + 1) + "/" + year);
            }
        };

        //Odabir datuma kraja.
        EditText endDate = root.findViewById(R.id.date_trip_end_date);

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.DateDialogTheme, endDateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        endDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                endDate.setText("" + dayOfMonth + "/" + (month + 1) + "/" + year);
            }
        };

        //Kreiranje i unos lokacije u text file.
        buttonAddNewTrip= root.findViewById(R.id.button_add_new_trip);

        buttonAddNewTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dohvati podatke.
                EditText name = root.findViewById(R.id.text_trip_name_input);
                String nameString = name.getText().toString();

                int locationID;
                Spinner location = root.findViewById(R.id.dropdown_trip_location);
                if (location.getSelectedItem() != null) {
                    locationID = location.getSelectedItemPosition();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_location), Toast.LENGTH_SHORT).show();
                    return;
                }

                EditText startDate = root.findViewById(R.id.date_trip_start_date);
                String startDateString = startDate.getText().toString();
                EditText endDate = root.findViewById(R.id.date_trip_end_date);
                String endDateString = endDate.getText().toString();
                EditText note = root.findViewById(R.id.text_trip_note_input);
                String noteString = note.getText().toString();

                if (TextUtils.isEmpty(name.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_trip_name), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(startDate.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_start_date), Toast.LENGTH_SHORT).show();
                    return;
                }

                //Stvori Trip.
                addNewTrip(nameString, locationID, startDateString, endDateString, noteString);

                dismiss();
            }
        });
        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        tripsList = db.tripDao().getAllTrips();
        locationsList = db.locationDao().getAllLocations();
    }

    private void addNewTrip(String name, int locationID, String startDate, String endDate, String noteString){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());

        Trip trip = new Trip(name, locationID, startDate, endDate, noteString);
        db.tripDao().insertTrip(trip);
    }

    private void fillLocationDropdown(View root){
        Spinner locationsDropdown = root.findViewById(R.id.dropdown_trip_location);
        String[] items = new String[locationsList.size()];

        int i = 0;
        for (Location l: locationsList
        ) {
            items[i] = l.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        locationsDropdown.setAdapter(adapter);
    }
}
