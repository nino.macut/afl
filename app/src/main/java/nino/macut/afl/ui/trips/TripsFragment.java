package nino.macut.afl.ui.trips;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.fishermen.FishermenFragment;

public class TripsFragment extends Fragment {

    public static TripsFragment newInstance() {
        return new TripsFragment();
    }

    private List<Trip> tripsList;
    private List<Fisherman> fishermenList;
    private List<Location> locationsList;

    private ListView listOfTrips;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_trips, container, false);

        verifyPermissions();
        loadLists();
        initializeList(root);
        initializeAddButton(root);

        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        tripsList = db.tripDao().getAllTrips();
        fishermenList = db.fishermanDao().getAllFishermen();
        locationsList = db.locationDao().getAllLocations();
    }



    private void initializeList(View root){
        listOfTrips = root.findViewById(R.id.list_trips);
        listOfTrips.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt("editID", position);
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_edit_trip, bundle);
            }
        });
        loadList();
    }

    private void initializeAddButton(View root){
        Button buttonAddTrip = root.findViewById(R.id.button_add_trip);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        buttonAddTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_add_trip);
            }
        });
    }

    private void loadList(){
        TripsAdapter tripsAdapter = new TripsAdapter(getActivity(), tripsList);
        listOfTrips.setAdapter(tripsAdapter);
    }

    public class TripsAdapter extends ArrayAdapter<Trip> {
        public TripsAdapter(Context context, List<Trip> tripsList){
            super(context, R.layout.listitem_trip, tripsList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Trip trip = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_trip,parent,false);
            }

            TextView name = convertView.findViewById(R.id.text_trip_name_listitem);
            TextView location = convertView.findViewById(R.id.text_trip_location_listitem);
            TextView duration = convertView.findViewById(R.id.text_trip_duration_listitem);

            name.setText(trip.name);

            Location foundLocation = null;
            for (Location l : locationsList){
                if (l.ID == (trip.location + 1)){
                    foundLocation = l;
                    break;
                }
            }
            if(foundLocation!=null){
                location.setText(foundLocation.name);
            }

            if (!trip.startDate.equals(trip.endDate)){
                duration.setText(trip.startDate + " - " + trip.endDate);
            }
            if (trip.startDate.equals(trip.endDate) || trip.endDate.equals("")){
                duration.setText(trip.startDate);
            }

            return convertView;
        }
    }
}