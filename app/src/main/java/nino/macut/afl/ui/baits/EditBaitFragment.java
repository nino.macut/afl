package nino.macut.afl.ui.baits;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.db.AppDatabase;

public class EditBaitFragment extends Fragment {

    private ImageView portrait;
    private Button buttonUploadBaitImage;
    private EditText dateOfBirth;
    private Button buttonEditBait;
    private Button buttonDeleteBait;
    private Button buttonAddNewCategory;
    private List<Bait> baitsList;
    private List<BaitCategory> baitCategoriesList;
    private int editID;
    private String fileName;

    int SELECT_IMAGE_CODE = 1;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_edit_bait, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadLists();
        fillBaitCategoriesDropdown(root);
        fillTypeDropdown(root);

        //Odabir profilne slike.
        portrait = root.findViewById(R.id.image_bait_preview);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            editID = bundle.getInt("editID");
            Bait bait = baitsList.get(editID);
            Bitmap bitmap = loadImageFromStorage(bait.image);
            portrait.setImageBitmap(bitmap);


            EditText name = root.findViewById(R.id.text_bait_name_input);
            name.setText(bait.name);

            EditText description = root.findViewById(R.id.text_bait_description_input);
            description.setText(bait.description);

            Spinner typeDropdown = root.findViewById(R.id.dropdown_bait_type);
            typeDropdown.setSelection(((ArrayAdapter)typeDropdown.getAdapter()).getPosition(bait.type));

            EditText color = root.findViewById(R.id.text_bait_color_input);
            color.setText(bait.color);

            EditText brand = root.findViewById(R.id.text_bait_brand_input);
            brand.setText(bait.brand);

            EditText size = root.findViewById(R.id.text_bait_size_input);
            if (bait.size == 0){
                size.setText("");
            } else {
                size.setText(Float.toString(bait.size));
            }
        }

        buttonUploadBaitImage = root.findViewById(R.id.button_upload_bait_image);

        buttonUploadBaitImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Title"), SELECT_IMAGE_CODE);
            }
        });

        buttonAddNewCategory = root.findViewById(R.id.button_add_new_bait_category);

        buttonAddNewCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();

                //Dohvati podatke.
                Bitmap bitmap;
                Boolean hasPicture = false;
                try{
                    BitmapDrawable image = (BitmapDrawable)portrait.getDrawable();
                    bitmap = image.getBitmap();
                    hasPicture = true;
                }
                catch (Exception e){
                    e.printStackTrace();
                    bitmap = null;
                    hasPicture = false;
                }

                int categoryID;
                Spinner category = root.findViewById(R.id.dropdown_bait_bait_category);
                if (category.getSelectedItem() != null) {
                    categoryID = category.getSelectedItemPosition();
                } else {
                    categoryID = 0;
                }

                EditText name = root.findViewById(R.id.text_bait_name_input);
                String nameString = name.getText().toString();

                EditText description = root.findViewById(R.id.text_bait_description_input);
                String descriptionString = description.getText().toString();

                Spinner typeDropdown = root.findViewById(R.id.dropdown_bait_type);
                String typeString = typeDropdown.getSelectedItem().toString();

                EditText color = root.findViewById(R.id.text_bait_color_input);
                String colorString = color.getText().toString();

                EditText brand = root.findViewById(R.id.text_bait_brand_input);
                String brandString = brand.getText().toString();

                EditText size = root.findViewById(R.id.text_bait_size_input);
                float sizeFloat;
                if (!size.getText().toString().isEmpty()){
                    sizeFloat = Float.parseFloat(size.getText().toString());
                } else {
                    sizeFloat = 0;
                }


                fileName = "bait" + nameString + ".jpg";

                if(hasPicture == true){
                    saveToInternalStorage(bitmap, fileName);
                }
                else if (hasPicture == false){
                    fileName = "nofile";
                }

                bundle.putString("fileName", fileName);
                bundle.putInt("categoryID", categoryID);
                bundle.putString("nameString", nameString);
                bundle.putString("descriptionString", descriptionString);
                bundle.putString("typeString", typeString);
                bundle.putString("brandString", brandString);
                bundle.putString("colorString", colorString);
                bundle.putFloat("sizeFloat", sizeFloat);
                bundle.putBoolean("editing", true);

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_bait_categories, bundle);
            }
        });

        //Brisanje ribiča
        buttonDeleteBait = root.findViewById(R.id.button_delete_bait);

        buttonDeleteBait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle(R.string.menu_confirm);
                builder.setMessage(R.string.message_are_you_sure_delete);

                builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Bait bait = baitsList.get(editID);
                        removeBaitFromDB(bait);
                        dialog.dismiss();
                        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                        navController.navigate(R.id.nav_baits);
                    }
                });

                builder.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        //Kreiranje i unos fisherman-a u text file.
        buttonEditBait = root.findViewById(R.id.button_edit_bait);

        buttonEditBait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dohvati podatke.
                Bitmap bitmap;
                Boolean hasPicture = false;
                try{
                    BitmapDrawable image = (BitmapDrawable)portrait.getDrawable();
                    bitmap = image.getBitmap();
                    hasPicture = true;
                }
                catch (Exception e){
                    e.printStackTrace();
                    bitmap = null;
                    hasPicture = false;
                }

                int categoryID;
                Spinner category = root.findViewById(R.id.dropdown_bait_bait_category);
                if (category.getSelectedItem() != null) {
                    categoryID = category.getSelectedItemPosition();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_category), Toast.LENGTH_SHORT).show();
                    return;
                }

                EditText name = root.findViewById(R.id.text_bait_name_input);
                String nameString = name.getText().toString();

                EditText description = root.findViewById(R.id.text_bait_description_input);
                String descriptionString = description.getText().toString();

                Spinner typeDropdown = root.findViewById(R.id.dropdown_bait_type);
                String typeString = typeDropdown.getSelectedItem().toString();

                EditText color = root.findViewById(R.id.text_bait_color_input);
                String colorString = color.getText().toString();

                EditText brand = root.findViewById(R.id.text_bait_brand_input);
                String brandString = brand.getText().toString();

                EditText size = root.findViewById(R.id.text_bait_size_input);
                float sizeFloat;
                if (!size.getText().toString().isEmpty()){
                    sizeFloat = Float.parseFloat(size.getText().toString());
                } else {
                    sizeFloat = 0;
                }

                if (TextUtils.isEmpty(name.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_bait_name), Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(color.getText())){
                    Toast.makeText(getActivity(), getString(R.string.toast_no_bait_color), Toast.LENGTH_SHORT).show();
                    return;
                }

                //Spremi sliku u internu memoriju.
                fileName = "bait" + nameString + ".jpg";

                if(hasPicture == true){
                    saveToInternalStorage(bitmap, fileName);
                }
                else if (hasPicture == false){
                    fileName = "nofile";
                }

                Bait bait = baitsList.get(editID);

                bait.category = categoryID;
                bait.image = fileName;
                bait.name = nameString;
                bait.description = descriptionString;
                bait.type = typeString;
                bait.color = colorString;
                bait.brand = brandString;
                bait.size = sizeFloat;

                editBaitInDB(bait);

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_baits);
            }
        });
        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        baitsList = db.baitDao().getAllBaits();
        baitCategoriesList = db.baitCategoryDao().getAllBaitCategories();
    }

    private void editBaitInDB(Bait bait){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.baitDao().updateBait(bait);
    }

    private void removeBaitFromDB(Bait bait){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.baitDao().deleteBait(bait);
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Baits", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String fileName){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Baits", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    private void fillTypeDropdown(View root){
        Spinner typeDropdown = root.findViewById(R.id.dropdown_bait_type);
        String[] items = getActivity().getResources().getStringArray(R.array.array_types_of_baits);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        typeDropdown.setAdapter(adapter);
    }

    private void fillBaitCategoriesDropdown(View root){
        Spinner baitCategoriesDropdown = root.findViewById(R.id.dropdown_bait_bait_category);
        String[] items = new String[baitCategoriesList.size()];

        int i = 0;
        for (BaitCategory b: baitCategoriesList
        ) {
            items[i] = b.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        baitCategoriesDropdown.setAdapter(adapter);
    }

    //Handler upita.
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_IMAGE_CODE){
            Uri uri = data.getData();
            portrait.setImageURI(uri);
        }
    }
}