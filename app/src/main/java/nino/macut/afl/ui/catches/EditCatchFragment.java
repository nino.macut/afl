package nino.macut.afl.ui.catches;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import nino.macut.afl.R;
import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Species;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.baits.AddBaitDialogFragment;
import nino.macut.afl.ui.fishermen.AddFishermanDialogFragment;
import nino.macut.afl.ui.species.AddSpeciesDialogFragment;
import nino.macut.afl.ui.trips.AddTripDialogFragment;

public class EditCatchFragment extends Fragment {

    private ImageView previewImage;
    private Button buttonUploadCatchImage;
    private Button buttonEditCatch;
    private Button buttonDeleteCatch;
    private Button buttonAddNewTrip;
    private Button buttonAddNewSpecies;
    private Button buttonAddNewBait;
    private Button buttonAddNewFisherman;

    EditText time;
    int hour;
    int minute;

    ImageView portrait;

    int editID;

    private List<Trip> tripsList;
    private List<Bait> baitsList;
    private List<Species> speciesList;
    private List<Fisherman> fishermenList;
    private List<Catch> catchesList;

    int SELECT_IMAGE_CODE = 1;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_catch, container, false);

        //Učitaj podatke iz datoteke.
        verifyPermissions();
        loadLists();
        fillDropdowns(root);

        portrait = root.findViewById(R.id.image_catch_preview);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            editID = bundle.getInt("editID");
            Catch ctch = catchesList.get(editID);

            Bitmap bitmap = loadImageFromStorage(ctch.image);
            portrait.setImageBitmap(bitmap);

            Spinner fishermanSpinner = root.findViewById(R.id.dropdown_catch_fisherman);
            int f = 0;
            for (Fisherman fisherman : fishermenList){
                if (fisherman.ID == ctch.fisherman){
                    break;
                }
                f++;
            }
            fishermanSpinner.setSelection(f);

            Spinner tripSpinner = root.findViewById(R.id.dropdown_catch_trip);
            int t = 0;
            for (Trip trip : tripsList){
                if (trip.ID == ctch.trip){
                    break;
                }
                t++;
            }
            tripSpinner.setSelection(t);

            Spinner baitSpinner = root.findViewById(R.id.dropdown_catch_bait);
            int b = 0;
            for (Bait bait : baitsList){
                if (bait.ID == ctch.bait){
                    break;
                }
                b++;
            }
            baitSpinner.setSelection(b);

            Spinner speciesSpinner = root.findViewById(R.id.dropdown_catch_species);
            int s = 0;
            for (Species species : speciesList){
                if (species.ID == ctch.species){
                    break;
                }
                s++;
            }
            speciesSpinner.setSelection(s);

            Spinner fishingMethod = root.findViewById(R.id.dropdown_catch_fishing_method);
            fishingMethod.setSelection(((ArrayAdapter)fishingMethod.getAdapter()).getPosition(ctch.fishingMethod));

            Spinner skyConditions = root.findViewById(R.id.dropdown_catch_sky_conditions);
            skyConditions.setSelection(((ArrayAdapter)skyConditions.getAdapter()).getPosition(ctch.skyConditions));

            Spinner waterClarity = root.findViewById(R.id.dropdown_catch_water_clarity);
            waterClarity.setSelection(((ArrayAdapter)waterClarity.getAdapter()).getPosition(ctch.waterClarity));

            EditText temperature = root.findViewById(R.id.text_catch_temperature_input);
            if (ctch.temperature == 0){
                temperature.setText("");
            } else {
                temperature.setText(String.valueOf(ctch.temperature));
            }

            EditText atmosphericPressure = root.findViewById(R.id.text_catch_atmospheric_pressure_input);
            if (ctch.atmosphericPressure == 0){
                atmosphericPressure.setText("");
            } else {
                atmosphericPressure.setText(String.valueOf(ctch.atmosphericPressure));
            }

            EditText wind = root.findViewById(R.id.text_catch_wind_input);
            if (ctch.wind == 0){
                wind.setText("");
            } else {
                wind.setText(String.valueOf(ctch.wind));
            }

            EditText length = root.findViewById(R.id.text_catch_length_input);
            if (ctch.length == 0){
                length.setText("");
            } else {
                length.setText(String.valueOf(ctch.length));
            }

            EditText weight = root.findViewById(R.id.text_catch_weight_input);
            if (ctch.weight == 0){
                weight.setText("");
            } else {
                weight.setText(String.valueOf(ctch.weight));
            }

            EditText waterDepth = root.findViewById(R.id.text_catch_water_depth);
            if (ctch.waterDepth == 0){
                waterDepth.setText("");
            } else {
                waterDepth.setText(String.valueOf(ctch.waterDepth));
            }

            EditText quantity = root.findViewById(R.id.text_catch_quantity_input);
            if (ctch.quantity == 0){
                quantity.setText("");
            } else {
                quantity.setText(String.valueOf(ctch.quantity));
            }

            EditText waterTemperature = root.findViewById(R.id.text_catch_water_temperature);
            if (ctch.waterTemperature == 0){
                waterTemperature.setText("");
            } else {
                waterTemperature.setText(String.valueOf(ctch.waterTemperature));
            }

            CheckBox released = root.findViewById(R.id.checkbox_catch_released);
            released.setChecked(ctch.released);

            EditText time = root.findViewById(R.id.text_catch_time_input);
            time.setText(ctch.time);
        }

        time = root.findViewById(R.id.text_catch_time_input);

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popTimePicker(root);
            }
        });

        //Odabir profilne slike.
        previewImage = root.findViewById(R.id.image_catch_preview);
        buttonUploadCatchImage = root.findViewById(R.id.button_upload_catch_portrait);

        buttonUploadCatchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Title"), SELECT_IMAGE_CODE);
            }
        });

        buttonAddNewTrip = root.findViewById(R.id.button_add_new_trip_from_catches);

        buttonAddNewTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTripDialogFragment df = new AddTripDialogFragment();
                df.show(getChildFragmentManager(), "AddTripDialogFragment");
            }
        });

        buttonAddNewSpecies = root.findViewById(R.id.button_add_new_species_from_catches);

        buttonAddNewSpecies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddSpeciesDialogFragment df = new AddSpeciesDialogFragment();
                df.show(getChildFragmentManager(), "AddSpeciesDialogFragment");
            }
        });

        buttonAddNewBait = root.findViewById(R.id.button_add_new_bait_from_catches);

        buttonAddNewBait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddBaitDialogFragment df = new AddBaitDialogFragment();
                df.show(getChildFragmentManager(), "AddBaitDialogFragment");
            }
        });

        buttonAddNewFisherman = root.findViewById(R.id.button_add_new_fisherman_from_catches);

        buttonAddNewFisherman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddFishermanDialogFragment df = new AddFishermanDialogFragment();
                df.show(getChildFragmentManager(), "AddFishermanDialogFragment");
            }
        });

        Spinner trip = root.findViewById(R.id.dropdown_catch_trip);

        trip.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadLists();
                    fillTripDropdown(root);
                    return false;
                }
                return false;
            }
        });

        Spinner species = root.findViewById(R.id.dropdown_catch_species);

        species.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadLists();
                    fillSpeciesDropdown(root);
                    return false;
                }
                return false;
            }
        });

        Spinner bait = root.findViewById(R.id.dropdown_catch_bait);

        bait.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadLists();
                    fillBaitDropdown(root);
                    return false;
                }
                return false;
            }
        });

        Spinner fisherman = root.findViewById(R.id.dropdown_catch_fisherman);

        fisherman.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP){
                    loadLists();
                    fillFishermenDropdown(root);
                    return false;
                }
                return false;
            }
        });

        buttonDeleteCatch = root.findViewById(R.id.button_delete_catch);

        buttonDeleteCatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setTitle(R.string.menu_confirm);
                builder.setMessage(R.string.message_are_you_sure_delete);

                builder.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Catch ctch = catchesList.get(editID);
                        removeCatchFromDB(ctch);
                        dialog.dismiss();
                        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                        navController.navigate(R.id.nav_catches);
                    }
                });

                builder.setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        //Kreiranje i unos lokacije u text file.
        buttonEditCatch = root.findViewById(R.id.button_edit_catch);

        buttonEditCatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dohvati podatke.
                Bitmap bitmap;
                Boolean hasPicture = false;
                try{
                    BitmapDrawable image = (BitmapDrawable)previewImage.getDrawable();
                    bitmap = image.getBitmap();
                    hasPicture = true;
                }
                catch (Exception e){
                    e.printStackTrace();
                    bitmap = null;
                    hasPicture = false;
                }

                int fishermanID;
                Spinner fisherman = root.findViewById(R.id.dropdown_catch_fisherman);
                if (fisherman.getSelectedItem() != null) {
                    fishermanID = fishermenList.get(fisherman.getSelectedItemPosition()).ID;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_fisherman), Toast.LENGTH_SHORT).show();
                    return;
                }

                int tripID;
                Spinner trip = root.findViewById(R.id.dropdown_catch_trip);
                if (trip.getSelectedItem() != null) {
                    tripID = tripsList.get(trip.getSelectedItemPosition()).ID;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_trip), Toast.LENGTH_SHORT).show();
                    return;
                }

                int baitID;
                Spinner bait = root.findViewById(R.id.dropdown_catch_bait);
                if (bait.getSelectedItem() != null) {
                    baitID = baitsList.get(bait.getSelectedItemPosition()).ID;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_bait), Toast.LENGTH_SHORT).show();
                    return;
                }

                int speciesID;
                Spinner species = root.findViewById(R.id.dropdown_catch_species);
                if (species.getSelectedItem() != null) {
                    speciesID = speciesList.get(species.getSelectedItemPosition()).ID;
                } else {
                    Toast.makeText(getActivity(), getString(R.string.toast_no_selected_species), Toast.LENGTH_SHORT).show();
                    return;
                }

                Spinner fishingMethod = root.findViewById(R.id.dropdown_catch_fishing_method);
                String fishingMethodString = fishingMethod.getSelectedItem().toString();

                Spinner skyConditions = root.findViewById(R.id.dropdown_catch_sky_conditions);
                String skyConditionsString = skyConditions.getSelectedItem().toString();

                Spinner waterClarity = root.findViewById(R.id.dropdown_catch_water_clarity);
                String waterClarityString = waterClarity.getSelectedItem().toString();

                EditText temperature = root.findViewById(R.id.text_catch_temperature_input);

                float temperatureFloat;
                if (!temperature.getText().toString().isEmpty()){
                    temperatureFloat = Float.parseFloat(temperature.getText().toString());
                } else {
                    temperatureFloat = 0;
                }

                EditText atmosphericPressure = root.findViewById(R.id.text_catch_atmospheric_pressure_input);

                float atmosphericPressureFloat;
                if (!atmosphericPressure.getText().toString().isEmpty()){
                    atmosphericPressureFloat = Float.parseFloat(atmosphericPressure.getText().toString());
                } else {
                    atmosphericPressureFloat = 0;
                }

                EditText wind = root.findViewById(R.id.text_catch_wind_input);

                float windFloat;
                if (!wind.getText().toString().isEmpty()){
                    windFloat = Float.parseFloat(wind.getText().toString());
                } else {
                    windFloat = 0;
                }

                EditText length = root.findViewById(R.id.text_catch_length_input);

                float lengthFloat;
                if (!length.getText().toString().isEmpty()){
                    lengthFloat = Float.parseFloat(length.getText().toString());
                } else {
                    lengthFloat = 0;
                }

                EditText weight = root.findViewById(R.id.text_catch_weight_input);

                float weightFloat;
                if (!weight.getText().toString().isEmpty()){
                    weightFloat = Float.parseFloat(weight.getText().toString());
                } else {
                    weightFloat = 0;
                }

                EditText waterDepth = root.findViewById(R.id.text_catch_water_depth);

                float waterDepthFloat;
                if (!waterDepth.getText().toString().isEmpty()){
                    waterDepthFloat = Float.parseFloat(waterDepth.getText().toString());
                } else {
                    waterDepthFloat = 0;
                }

                EditText quantity = root.findViewById(R.id.text_catch_quantity_input);

                int quantityInt;
                if (!quantity.getText().toString().isEmpty()){
                    quantityInt = Integer.parseInt(quantity.getText().toString());
                } else {
                    quantityInt = 0;
                }

                EditText waterTemperature = root.findViewById(R.id.text_catch_water_temperature);

                float waterTemperatureFloat;
                if (!waterTemperature.getText().toString().isEmpty()){
                    waterTemperatureFloat = Float.parseFloat(waterTemperature.getText().toString());
                } else {
                    waterTemperatureFloat = 0;
                }

                CheckBox released = root.findViewById(R.id.checkbox_catch_released);
                boolean releasedBoolean = released.isChecked();

                //Spremi sliku u internu memoriju.
                String fileName = "catch" + getRandomNumber(0,100000) + ".jpg";

                if(hasPicture == true){
                    saveToInternalStorage(bitmap, fileName);
                }
                else if (hasPicture == false){
                    fileName = "nofile";
                }

                EditText time = root.findViewById(R.id.text_catch_time_input);
                String timeString = time.getText().toString();

                Catch ctch = catchesList.get(editID);

                ctch.image = fileName;
                ctch.time = timeString;
                ctch.species = speciesID;
                ctch.fisherman = fishermanID;
                ctch.trip = tripID;
                ctch.bait = baitID;
                ctch.temperature = temperatureFloat;
                ctch.atmosphericPressure = atmosphericPressureFloat;
                ctch.wind = windFloat;
                ctch.skyConditions = skyConditionsString;
                ctch.length = lengthFloat;
                ctch.weight = weightFloat;
                ctch.quantity = quantityInt;
                ctch.waterClarity = waterClarityString;
                ctch.waterDepth = waterDepthFloat;
                ctch.waterTemperature = waterTemperatureFloat;
                ctch.fishingMethod = fishingMethodString;
                ctch.released = releasedBoolean;

                editCatchInDB(ctch);

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_catches);
            }
        });

        return root;
    }

    //Handler upita.
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_IMAGE_CODE){
            Uri uri = data.getData();
            previewImage.setImageURI(uri);
        }
    }

    private void removeCatchFromDB(Catch ctch){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.catchDao().deleteCatch(ctch);
    }

    private void editCatchInDB(Catch ctch){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        db.catchDao().updateCatch(ctch);
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadLists(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        tripsList = db.tripDao().getAllTrips();
        speciesList = db.speciesDao().getAllSpecies();
        baitsList = db.baitDao().getAllBaits();
        fishermenList = db.fishermanDao().getAllFishermen();
        catchesList = db.catchDao().getAllCatches();
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Catches", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private void fillTripDropdown(View root){
        Spinner tripsDropdown = root.findViewById(R.id.dropdown_catch_trip);
        String[] items = new String[tripsList.size()];

        int i = 0;
        for (Trip t: tripsList
        ) {
            items[i] = t.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        tripsDropdown.setAdapter(adapter);
    }

    private void fillFishermenDropdown(View root){
        Spinner fishermenDropdown = root.findViewById(R.id.dropdown_catch_fisherman);
        String[] items = new String[fishermenList.size()];

        int i = 0;
        for (Fisherman f: fishermenList
        ) {
            items[i] = f.firstName + " " + f.lastName;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        fishermenDropdown.setAdapter(adapter);
    }

    private void fillBaitDropdown(View root){
        Spinner baitsDropdown = root.findViewById(R.id.dropdown_catch_bait);
        String[] items = new String[baitsList.size()];

        int i = 0;
        for (Bait b: baitsList
        ) {
            items[i] = b.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        baitsDropdown.setAdapter(adapter);
    }

    private void fillSpeciesDropdown(View root){
        Spinner speciesDropdown = root.findViewById(R.id.dropdown_catch_species);
        String[] items = new String[speciesList.size()];

        int i = 0;
        for (Species s: speciesList
        ) {
            items[i] = s.name;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        speciesDropdown.setAdapter(adapter);
    }

    private void fillWaterClarityDropdown(View root){
        Spinner waterClarityDropdown = root.findViewById(R.id.dropdown_catch_water_clarity);
        String[] items = getActivity().getResources().getStringArray(R.array.array_water_clarity);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        waterClarityDropdown.setAdapter(adapter);
    }

    private void fillSkyConditionsDropdown(View root){
        Spinner skyConditionsDropdown = root.findViewById(R.id.dropdown_catch_sky_conditions);
        String[] items = getActivity().getResources().getStringArray(R.array.array_sky_conditions);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        skyConditionsDropdown.setAdapter(adapter);
    }

    private void fillFishingMethodDropdown(View root){
        Spinner fishingMethodDropdown = root.findViewById(R.id.dropdown_catch_fishing_method);
        String[] items = getActivity().getResources().getStringArray(R.array.array_fishing_methods);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        fishingMethodDropdown.setAdapter(adapter);
    }

    private void fillDropdowns(View root){
        fillFishermenDropdown(root);
        fillBaitDropdown(root);
        fillFishingMethodDropdown(root);
        fillSkyConditionsDropdown(root);
        fillSpeciesDropdown(root);
        fillTripDropdown(root);
        fillWaterClarityDropdown(root);
    }

    private int getRandomNumber(int min,int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String fileName){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Catches", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    public void popTimePicker(View root){
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
                hour = selectedHour;
                minute = selectedMinute;
                time.setText(String.format(Locale.getDefault(), "%02d:%02d",hour,minute));
            }
        };


        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.DialogTheme, onTimeSetListener, hour, minute, true);

        timePickerDialog.setTitle(R.string.input_select_time);
        timePickerDialog.show();
    }
}
