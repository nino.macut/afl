package nino.macut.afl.ui.species;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import nino.macut.afl.R;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.Species;
import nino.macut.afl.db.AppDatabase;
import nino.macut.afl.ui.fishermen.FishermenFragment;

public class SpeciesFragment extends Fragment {

    private List<Species> speciesList;
    private ListView listOfSpecies;

    private static final int REQUEST_CODE = 1;
    private static final String[] STORAGE_PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_species, container, false);

        verifyPermissions();
        loadSpeciesList();
        initializeList(root);
        initializeAddButton(root);

        return root;
    }

    private void verifyPermissions(){
        int permissionRead = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWrite = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if ((permissionRead != PackageManager.PERMISSION_GRANTED) || (permissionWrite != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(getActivity(),STORAGE_PERMISSIONS, REQUEST_CODE);
        }
    }

    private void loadSpeciesList(){
        AppDatabase db = AppDatabase.getDbInstance(this.getActivity());
        speciesList = db.speciesDao().getAllSpecies();
    }

    private void loadList(){
        SpeciesAdapter speciesAdapter = new SpeciesAdapter(getActivity(), speciesList);
        listOfSpecies.setAdapter(speciesAdapter);
    }

    private void initializeList(View root){
        listOfSpecies = root.findViewById(R.id.list_species);
        listOfSpecies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt("editID", position);
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.nav_edit_species, bundle);
            }
        });
        loadList();
    }

    private void initializeAddButton(View root){
        Button buttonAddSpecies = root.findViewById(R.id.button_add_species);
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        buttonAddSpecies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_add_species);
            }
        });
    }

    private Bitmap loadImageFromStorage(String fileName)
    {
        ContextWrapper cw = new ContextWrapper(getActivity());
        File directory = cw.getDir("Species", Context.MODE_PRIVATE);
        try {
            File f=new File(directory, fileName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public class SpeciesAdapter extends ArrayAdapter<Species> {
        public SpeciesAdapter(Context context, List<Species> speciesList){
            super(context, R.layout.listitem_species, speciesList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Species species = getItem(position);

            if (convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_species,parent,false);
            }

            ImageView image = convertView.findViewById(R.id.image_species_preview);
            TextView name = convertView.findViewById(R.id.text_species_name);
            TextView type = convertView.findViewById(R.id.text_species_type);

            if (!species.image.equals("nofile")){
                Bitmap bitmap = loadImageFromStorage(species.image);
                image.setImageBitmap(bitmap);
            }
            else if (species.image.equals("nofile")){
                image.setImageResource(R.drawable.ic_species_no_pic);
            }

            name.setText(species.name + "");

            type.setText(species.type);

            return convertView;
        }
    }
}