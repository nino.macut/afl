package nino.macut.afl.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import nino.macut.afl.classes.Bait;
import nino.macut.afl.classes.BaitCategory;
import nino.macut.afl.classes.BaitCategoryDao;
import nino.macut.afl.classes.BaitDao;
import nino.macut.afl.classes.Catch;
import nino.macut.afl.classes.CatchDao;
import nino.macut.afl.classes.Fisherman;
import nino.macut.afl.classes.FishermanDao;
import nino.macut.afl.classes.Location;
import nino.macut.afl.classes.LocationDao;
import nino.macut.afl.classes.Species;
import nino.macut.afl.classes.SpeciesDao;
import nino.macut.afl.classes.Spot;
import nino.macut.afl.classes.SpotDao;
import nino.macut.afl.classes.Trip;
import nino.macut.afl.classes.TripDao;

@Database(entities = {Fisherman.class, Location.class, Spot.class, Trip.class, Species.class, BaitCategory.class, Bait.class, Catch.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract FishermanDao fishermanDao();
    public abstract LocationDao locationDao();
    public abstract SpotDao spotDao();
    public abstract TripDao tripDao();
    public abstract SpeciesDao speciesDao();
    public abstract BaitCategoryDao baitCategoryDao();
    public abstract BaitDao baitDao();
    public abstract CatchDao catchDao();

    private static AppDatabase INSTANCE;

    public static AppDatabase getDbInstance(Context context){
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "DB_DB16").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }
}